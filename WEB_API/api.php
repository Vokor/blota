<?php
	/*
		Blota API V1.0
			By Fluffy
			
		Requires MySQL 5 or higher
	*/
	
	// Database information
		$DBHost = "localhost";
		$DBUsername = "root";
		$DBPassword = "";
		$DBDatabase = "blotaAPI";
		
	// Blota Updator Prefs
		$BlotaVersionNumber = "0";
		$BlotaVersionName = "Alpha 3.2";
		$BlotaChangeLog = "";
		$PathToBlota = "blota.zip";
		
	// Wallpaper System
		$WallpaperCount = 0;
		
	// DefaultView
		$defaultView = "<html>
						  <head>
							  <link rel=\"stylesheet\" href=\"style.css\">
						  </head>
						 <body>
							  <center><img src=\"logo.png\" width=\"500\" height=\"600\"></center>
						 </body>
					   </html>";
	// End vars

	$Mode = $_POST['MODE'];
	
	function send_AuthReq($name, $ip) 
	{
		$res = http_post_fields("http://auth.blockland.us/authQuery.php", array(
			"NAME" => $name,
			"IP" => $ip
		));

		$body = http_parse_message($res)->body;
		$explodedBody = explode(' ', $body);
				
		if($explodedBody[0] == "YES")
		{
			return $explodedBody[1];
		}
	}
	
	if($Mode == "AUTH")
	{
		echo("100 Initiating AUTH Request\n");
		$Name = $_POST['NAME'];
		$IP = $_SERVER['REMOTE_ADDR'];
		
		$result = send_AuthReq($Name, $IP);
		
		if($result != "")
		{
			mysql_connect($DBHost, $DBUsername, $DBPassword) or die(mysql_error());
			mysql_select_db($DBDatabase) or die(mysql_error());
			$date = date("Y-m-d H:i:s");
		
			mysql_query("INSERT INTO users (Name, BLID, IP) VALUES('$Name', '$result', '$IP')") or mysql_query("UPDATE users SET Name = '$Name', IP = '$IP', LastLogin = '$date' WHERE BLID = '$result'");
			
			echo("101 AUTH SUCCESS!\n");
		} else {
			echo("102 AUTH FALIURE\n");
		}
	}
	
	if($Mode == "VNUM")
	{
		echo("103 BlotaVersion: " . $BlotaVersionNumber . "\n");
	}
	
	if($Mode == "DOWNLOAD")
	{	
		$size = filesize($PathToBlota);
		header('Blota-Version: ' . $BlotaVersionName);
		header('Content-type: application/octet-stream');
		header('Content-Length: ' . $size);
		
		echo file_get_contents($PathToBlota);
	}
	
	if($Mode == "GETWALLPREVIEWS")
	{
		print "104 Listing Img IDs\r\n";
		print "105 BlotaAPI/wallpapers/prev_";
		
		for ($i = 0; $i <= $WallpaperCount; $i++)
		{
    		echo "106 " . $i;
		}
		
		print "107 END IMG IDs";
	}
	
	if($Mode == "GETWALLPAPER")
	{
		$WALLID = $_POST['WALLID'];
		$RES = $_POST['RES'];
		
		$WallPath = "wallpapers/full_" . $WALLID . $RES . ".png";
		
		if(is_file($WallPath))
		{
			$size = filesize($WallPath);
			header('Content-type: image/png');
			header('Content-Length: ' . $size);
			echo file_get_contents($WallPath);
		} else {
			$WallPath = "wallpapers/full_" . $WALLID . "1024_768.png";
			$size = filesize($WallPath);
			header('Content-type: image/png');
			header('Content-Length: ' . $size);
			echo file_get_contents($WallPath);
		}
	}
	
	if($Mode == "")
	{
		echo($defaultView);
	}
?>