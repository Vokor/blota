exec("./nmhComplete.cs");

function comText_registerWord(%word)
{
	%list = $Pref::ComText::Words;
	if(hasItemOnList(%list,%word))
	{
		//nothing
	}
	else
	{
		$Pref::ComText::Words = addItemToList($Pref::ComText::Words,%word);
	}
}

function comText_findProgress(%word, %con)
{
	%len = strLen(%word);
		//echo("ComText:: Debug:: %len = " @ %len);
	%q = 0;
	while(%q < %len)
	{
		%q += 1;
		%progress = getSubStr(%word, 0, %q);
			//echo("ComTest:: Debug:: %progress = " @ %progress @ "  || & %q = " @ %q);
		if(%progress $= %con)
		{
				//echo("ComTest:: Debug:: went through for for if!");
			return 1;
			break;
		}
	}
	return 0;
}

function comText_getProgress(%word, %con, %this)
{
	%len = strLen(%word);
		//echo("ComText:: Debug:: GP %len = " @ %len);
	%q = 0;
	while(%q < %len)
	{
		%q += 1;
		%progress = getSubStr(%word, 0, %q);
			//echo("ComTest:: Debug:: GP %progress = " @ %progress @ "  || & %q = " @ %q);
		if(%progress $= %con)
		{
				//echo("ComTest:: Debug:: GP went through for for if!");
			%this.comText_lastWord = %word;
			return %progress;
			break;
		}
	}
	return 0;
}



package completeText
{
	function NMH_Type::type(%this)
	{
		Parent::type(%this);
		if(!%this.comText_Check)
		{
			%con = %this.getValue();
				//echo("ComText:: Debug:: %con = " @ %con);
			%list = $Pref::ComText::Words;
				//echo("ComText:: Debug:: %list = " @ %list);
			%count = getWordCount(%list);
				//echo("ComText:: Debug:: %count = " @ %count);
			%count2 = getWordCount(%con);
				//echo("ComText:: Debug:: %count2 = " @ %count2);
			%con2 = getWord(%con,%count2-1);
				//echo("ComText:: Debug:: %con2 = " @ %con2);
			for(%i = 0; %i < %count; %i++)
			{
				%word = getWord(%list,%i);
					//echo("ComText:: Debug:: %word = " @ %word);
					//echo("ComText:: Debug:: %lastWord = " @ %this.comText_lastWord);
				if(comText_findProgress(%word, %con2))
				{
					if(%this.comText_lastWord $= %word)
					{
						//nothing
					}
					else
					{
						%pro = comText_getProgress(%word, %con2, %this);
						%word2 = strReplace(%word,%pro,"");
							//echo("ComText:: Debug:: %word2 = " @ %word2);
						NMH_Type.setcompletionText(%word2);
						%this.comText_check = 1;
						break;
					}
				}
			}
			//if(hasItemOnList(%this,%word))
			//{
			//	NMH_Type.setcompletionText(%word);
			//}
		}
	}
	function NMH_Type::send(%this)
	{
		parent::send(%this);
		%this.comText_lastWord = "";
		%this.comText_check = 0;
	}

	function NMH_Type::onSleep(%this)
	{
		//parent::onSleep(%this);
		%this.comText_lastWord = "";
		%this.comText_check = 0;
	}

	function NMH_Type::failedType(%this)
	{
		%this.comText_Check = 0;
		//%this.comText_LastWord = "";
	}

	function playGui::onWake(%this,%obj)
	{
		parent::onWake(%this,%obj);
		$Pref::ComText::Words = "";
		%rows = NPL_List.rowCount();
    	for (%i = 0 ; %i < %rows ; %i++)
    	{
        	%line = NPL_List.getRowText(%i);

        	%blid = getField(%line, 3); //NPL_List.getRowId(%i);
        	%name = getField(%line, 1);

        	comText_registerWord(%name);
    	}
    	rect();

	}
};
activatePackage(completeText);

//exec("./nmhComplete.cs");

function rect()
{
	exec("./comText.cs");
}