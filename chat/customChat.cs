package customChat
{

	function clientCmdChatMessage(%a,%b,%c,%fmsg,%cp,%name,%cs,%msg)
	{
		if($Blota::Chat)
		{
			%cp = $Blota::Chat::ClanColor @ %cp;
			if(%name $= $Pref::Player::NetName){ %name = $Blota::Chat::SelfNameColor @ %name; %msg = $Blota::Chat::SelfMessageColor @ %msg; } else { %name = $Blota::Chat::NameColor @ %name; %msg = $Blota::Chat::MessageColor @ %msg; }
			%cs = $Blota::Chat::ClanColor @ %cs;
			%msg = $Blota::Chat::MessageColor @ %msg;
			%font = "<font:" @ $Blota::Chat::FontFamily @ ":" @ $Blota::Chat::FontSize @ ">";

			%fmsg = %font @ %cp @ %name @ %cs @ "\c6:" SPC %msg;
		}

		parent::clientCmdChatMessage(%a,%b,%c,%fmsg,%cp,%name,%cs,%msg);

	}


	function newChatSO::addLine(%a,%msg)
	{
		if($Blota::Chat)
		{

			if($Blota::Chat::Echo)
			{

				echo(%msg);

			}

			%add = "<font:" @ $Blota::Chat::FontFamily @ ":" @ $Blota::Chat::FontSize @ ">";
			%msg = %add @ %msg;
		}

		parent::addLine(%a,%msg);

	}

};
activatepackage(customChat);