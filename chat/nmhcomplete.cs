//small edits by munk for comtext

// HEY YOU
// If you're in here maybe you're a developer
// Well it's dangerous to go alone: take this VVV

// Usage: NMH_Type.setCompletionText(%string)
// Appends a grey 'suggestion' to the chat box.
// When the user types, if it matches the suggestion it will type along with it.
// If they backspace, it removes the suggestion.
// If they press enter, it inserts the suggestion as actual text.

// I had plans to use it in this, but I was too lazy to update it to work with lists. But someone else might have a use for it.
// Take this function and the package underneath it.
// Code licensed under WTFPL - http://sam.zoy.org/wtfpl/

function NMH_Type::setCompletionText(%this,%str)
{
	%con = %this.getValue();
	if(striPos(%con,"\c2") != -1)
	{
		%con = getSubStr(%con,0,striPos(%con,"\c2"));
	}
	if(%str !$= "")
	{
		%con = %con @ "\c2" @ %str;
	}
	%this.setValue(%con);
}

package NMHCompletion
{
	function NMH_Type::send(%this)
	{
		%con = %this.getValue();
		%pos = striPos(%con,"\c2");
		if(%pos != -1)
		{
			%this.setValue(getSubStr(%con,0,%pos) @ getSubStr(%con,%pos + 1,strLen(%con)));
		} else {
			Parent::send(%this);
		}
		%this.lastLen = strLen(%this.getValue());
	}
	function NMH_Type::type(%this)
	{
		Parent::type(%this);
		%con = %this.getValue();
		if(strLen(%con) == 0 && %this.lastLen == 0)
		{
			return;
		}
		if(strLen(%con) < %this.lastLen)
		{
			%char = "backspace";
		} else {
			%char = getSubStr(%con,strLen(%con) - 1,1);
			%con = getSubStr(%con,0,strLen(%con) - 1);
		}
		%pos = striPos(%con,"\c2");
		if(%pos != -1)
		{
			if(%char $= "backspace" || %char $= "\c2")
			{
				%this.setValue(getSubStr(%con,0,%pos));
			} else {
				%pre = getSubStr(%con,0,%pos);
				%post = getSubStr(%con,%pos + 1,strLen(%con));
				if(strPos(%char,getSubStr(%post,0,1)) == 0)
				{
					%con = %pre @ %char;
					if(strLen(%post) > 1)
					{
						%con = %con @ "\c2" @ getSubStr(%post,1,strlen(%post));
					}
				} else {
						//%this.comtext_Fail = 1;
					NMH_type.failedType();
					%con = %pre @ %char;
				}
				%this.setValue(%con);
			}
		} else {
			%this.lastLen = strLen(%con);
		}
	}
	function NMH_Type::setValue(%this,%val)
	{
		Parent::setValue(%this,%val);
		%this.lastLen = strLen(%val);
	}
};
activatePackage(NMHCompletion);
