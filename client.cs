//Blota
//A Mod
//Heavly based on Blockland Glass's Desktop and BlockOS

if(isFile("config/client/blota/prefs.cs"))
{
        exec("config/client/blota/prefs.cs");
}
else
{
        $Blota::StickyTime = 60000;
        $Blota::NotifCorner = 1;
        $blota::RTBNotif = 1;
        $blota::Desktop = 1;
        $Blota::TopBar::Desktop = 1;
        $Blota::Topbar::InGame = 1;
        $Blota::TopBar::Trans = 0;
        $blota::Topbar::hours = 0;
        $blota::newServerGui::downloadpreviews = true;
        $Blota::Wallpaper = "Add-Ons/System_Blota/images/walls/defaultWall.png";
        export("$Blota::*","config/client/blota/prefs.cs");

        //Custom Chat Prefs
        $Blota::Chat = 1;
        $Blota::Chat::FontFamily = "Palatino Linotype";
        $Blota::Chat::FontSize = 18;
        $Blota::Chat::NameColor = "\c3";
        $Blota::Chat::SelfNameColor = "<color:ffffaa>";
        $Blota::Chat::ClanColor = "\c7";
        $Blota::Chat::MessageColor = "\c6";
        $Blota::Chat::SelfMessageColor = "<color:d5d5d5>";
        $Blota::Chat::Echo = 0;
}

exec("./modules/profiles.cs");
exec("./modules/replacements.cs");
exec("./modules/APIMethods.cs");
exec("./modules/loadingGUI/main.cs");
exec("./modules/fonts/fonts.cs");
//exec("./modules/dashboard/dashboard.cs");
exec("./chat/customChat.cs");
exec("./chat/comText.cs");

$Blota::Version = "0.09";
//$Blota::Width = getWord($pref::video::resolution, 0);
//$Blota::Height = getWord($pref::video::resolution, 1);
//$Blota::Res = getWord($Pref::video::resolution, 0) SPC getWord($Pref::video::resolution, 1);
$Blota::ImagePath = "Add-ons/System_Blota/Images/";
$Blota::SoundPath = "Add-ons/System_Blota/sounds/";
$Blota::TopBarIcons = 0;

function blota_getTopBarTrans()
{
        if($Blota::TopBar::Trans)
        {
                %bitmap = $Blota::ImagePath @ "bar_trans.png";
                return %bitmap;
        }
        else
        {
                %bitmap = $Blota::ImagePath @ "bar.png";
                return %bitmap;
        }
}


exec("./Blota_Desktop.gui");
exec("./Blota_Options.gui");

Blota_DT_Swatch.add(Blota_APIPopup);
Blota_APIPopup.setVisible(1);

if(!isFile("config/client/blota/walls/defaultWall.png"))
{
        if(!isFile("config/client/blota/walls/info.txt"))
   {
      %fo = new FileObject();
      %fo.openForWrite("config/client/blota/walls/info.txt");
   
      %fo.writeLine("Wallpaper Help");
      %fo.writeLine("");
      %fo.writeLine("Blota Wallpaper must be in this folder and be in PNG or JPG format to be recognised");
   
      %fo.close();
      %fo.delete();
   }
   		%defaultwall = $Blota::ImagePath @ "walls/defaultWall.png";
   		%wall1 = $Blota::ImagePath @ "walls/1.png";
   		if(isFile(%defaulwall))
        	fileCopy(%defaultwall, "config/client/blota/walls/defaultWall.png");
        if(isFile(%wall1))
        	fileCopy(%wall1, "config/client/blota/walls/1.png");
        //fileCopy($Blota::ImagePath @ "walls/2.jpg", "config/client/blota/walls/2.jpg");
        //fileCopy($Blota::ImagePath @ "walls/3.jpg", "config/client/blota/walls/3.jpg");
        //fileCopy($Blota::ImagePath @ "walls/4.jpg", "config/client/blota/walls/4.jpg");
}

// .rezize( x, y, w, h);


function initiateBlotaDT()
{
        //Blota_Desktop.resize("0 0" SPC $pref::video::resolution);
        //Blota_DT_Swatch.resize("0 0" SPC $pref::video::resolution);
        //Blota_DT_Background.resize("0 0" SPC $pref::video::resolution);
        //Blota_DT_TopBar.resize("0 0" SPC $Blota::Width SPC "25");
        //Blota_DT_TopBarLine.resize("0 25" SPC $Blota::Width SPC "1");
        if($Blota::Desktop)
        {
        mainMenuGUI.setName("oldMainMenu");
        Blota_Desktop.setName("mainMenuGUI");
        oldMainMenu.extent = getWords(getRes(), 0, 1);
        canvas.popDialog(oldMainMenu);
        canvas.pushDialog(mainMenuGUI);
                if($Blota::TopBar::Desktop)
                {
                        mainMenuGUI.add(Blota_TopBar);
                }
        }
        else if($Blota::TopBar::Desktop)
        {
                mainMenuGui.add(Blota_TopBar);
        }

        $Blota::optionsMenu = 0;
        $Blota::GameMenu = 0;

        //Blota_Desktop_refresh();
        schedule(1000, 0, Blota_Desktop_timeLoop);
        //Blota_Desktop_rightLoop();

}
function execDir(%path)
{
        %filesearch = FindFirstFile(%dir @ "*.cs");
        while (strlen(%filesearch) > 0)
        {
                exec(%filesearch);
                %filesearch = FindNextFile(%path @ "*.cs");
        }
        %filesearch = FindFirstFile(%dir @ "*.gui");
        while (strlen(%filesearch) > 0)
        {
                exec(%filesearch);
                %filesearch = FindNextFile(%path @ "*.gui");
        }
}
execDir("Add-Ons/system_blota/scripts/");
schedule(1500, 0, initiateBlotaDT);
rect();