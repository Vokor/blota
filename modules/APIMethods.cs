// Blota API Networking & Updater
//  Author: Fluffy

//Update GUI
exec("./update.gui");

function Blota_SendRequest(%mode)
{
    if(!isObject(BlotaAPI_TCP))
        %tcp = new TCPObject(BlotaAPI_TCP);
    else
        %tcp = BlotaAPI_TCP;

        %tcp.host = "datafling.com";
        %tcp.port = "80";
        %tcp.path = "/BlotaAPI/api.php";
        %tcp.mode = %mode;
        %tcp.version = $Blota::Version;
        
        %tcp.debug = false;
        
    Blota_APIPopup_Text.setText("Connecting to Blota API...");
    
    %tcp.connect(%tcp.host @ ":" @ %tcp.port);
}

function BlotaAPI_TCP::onConnected(%this)
{
    if(%this.mode $= "AUTH")
    {
        %data = "MODE=AUTH&NAME=" @ $pref::Player::NetName;
        Blota_APIPopup_Text.setText("Sending Auth Packet...");
    }
    
    if(%this.mode $= "GETVERSION")
    {
        if(isFile("Add-Ons/System_Blota/.git/HEAD"))
        {
            %this.disconnect();
            return canvas.popDialog(Blota_APIPopup);
        }
        %data = "MODE=VNUM";
        Blota_APIPopup_Text.setText("Requesting Version Num...");
    }
    
    if(%this.mode $= "UPDATE")
    {
        %data = "MODE=DOWNLOAD";
        Blota_APIPopup_Text.setText("Initialising Download...");
    }
    
    if(%this.mode $= "WALLPAPERS")
    {
        %data = "MODE=GETWALLPREVIEWS";
    }
    
    %packet = "";
    %packet = %packet @ "POST " @ %this.path @ " HTTP/1.1\r\n";
    %packet = %packet @ "Host: " @ %this.host @ "\r\n";
    %packet = %packet @ "Content-Type: application/x-www-form-urlencoded\r\n";
    %packet = %packet @ "Content-Length: " @ strLen(%data) @ "\r\n\r\n";
    %packet = %packet @ %data @ "\r\n";
        
    %this.send(%packet);
}

function BlotaAPI_TCP::onLine(%this, %line)
{
    if(%this.debug)
        echo(%line);
    
    // Version
    if(%this.mode $= "GETVERSION" && getWord(%line, 0) $= "103" && getWord(%line, 2) > %this.version)
    {
        Blota_SendRequest("UPDATE");
        canvas.PushDialog(BlotaUpdaterGUI);
        BlotaUpdate_ProgressCtrl.setValue(0);
        BlotaUpdate_InfoBox.setValue("Downloading update info...");
        BlotaUpdate_window.extent = "434 87";
    }
    
    if(%this.mode $= "GETVERSION" && getWord(%line, 0) $= "103" && getWord(%line, 2) <= %this.version)
    {
        Blota_APIPopup_Text.setText("This installation is up to date!");
    }
    
    // Updater binary stuff
    if(getWord(%line, 0) $= "Content-Length:" && %this.mode $= "UPDATE")
    {
        %this.length = getword(%line, 1);
    }
    if(%line $= "" && %this.mode $= "UPDATE")
    {
        %this.setBinarySize(%this.length);
    }
    
    // Authentication
    if(getWord(%line, 0) $= "101")
    {
        echo("Blota API: Auth Success!");
        %this.disconnect();
        Blota_APIPopup_Text.setText("Authentication Success!");
        Blota_SendRequest("GETVERSION");
        Blota_APIPopup.schedule(2000, setVisible, 0);
    }
    
    if(getWord(%line, 0) $= "102")
    {
        echo("Blota API: Auth Faliure!");
        echo("   Blota master server may be down.");
        %this.disconnect();
        
        Blota_SendRequest("GETVERSION");
    }
    
    if(getWord(%line, 0) $= "Blota-Version:")
    {
        BlotaUpdate_InfoBox.setValue("Updating to Blota Version: " @ restWords(%line));
    }
    
    if(getWord(%line, 0) $= "105")
    {
        %this.prevPath = restWords(%line);
    }
    
    if(getWord(%line, 0) $= "106")
    {
        %this.wallIDs = %this.wallIDs TAB getWord(%line, 1);
    }
    
    if(getWord(%line, 0) $= "107")
    {
        BDash_LoadWallpaperPreviews(%this.prevPath, %this.wallIDs);
    }
}

function BlotaAPI_TCP::onBinChunk(%this, %chunk)
{
    %perCent = mFloatLength(%chunk / %this.length * 100, 0);
    %done = mFloatLength(%chunk / 1024, 0);
    %length = mFloatLength(%this.length /1024, 0);
    %progress = %chunk / %this.length;
    
    BlotaUpdate_ProgressCtrl.setValue(%progress);
    
    BlotaUpdate_DoneBox.setValue("<color:000000>Progress: " @ %perCent @ "% ( " @ %done @ " / " @ %length @ " kb )");
    
    if(%chunk >= %this.length) 
    {
            %this.saveBufferToFile("Add-Ons/System_Blota.zip");
            %this.disconnect();
            %this.setBinary(false);
            BlotaUpdate_window.extent = "434 128";
    }
}

package BlotaAuthPackage
{
   function MM_AuthBar::blinkSuccess(%this)
   {
      Parent::blinkSuccess(%this);
      Blota_SendRequest("AUTH");
   }
};
activatePackage(BlotaAuthPackage);

// Popup

new GuiSwatchCtrl(Blota_APIPopup) {
  profile = "GuiDefaultProfile";
  horizSizing = "left";
  vertSizing = "top";
  position = "393 462";
  extent = "256 19";
  minExtent = "8 2";
  enabled = "1";
  visible = "1";
  clipToParent = "1";
  color = "255 255 255 255";

  new GuiAnimatedBitmapCtrl() {
     profile = "GuiDefaultProfile";
     horizSizing = "right";
     vertSizing = "center";
     position = "9 4";
     extent = "16 11";
     minExtent = "8 2";
     enabled = "1";
     visible = "1";
     clipToParent = "1";
     bitmap = "~/System_Blota/images/ui/loader/flat";
     wrap = "0";
     lockAspectRatio = "0";
     alignLeft = "0";
     alignTop = "0";
     overflowImage = "0";
     keepCached = "0";
     mColor = "255 255 255 255";
     mMultiply = "0";
     framesPerSecond = "10";
     numFrames = "7";
     skipFrames = "0";
  };
  new GuiMLTextCtrl(Blota_APIPopup_Text) {
     profile = "BlotaBodyText";
     horizSizing = "right";
     vertSizing = "center";
     position = "37 3";
     extent = "252 12";
     minExtent = "8 2";
     enabled = "1";
     visible = "1";
     clipToParent = "1";
     lineSpacing = "2";
     allowColorChars = "0";
     maxChars = "-1";
     text = "Idle";
     maxBitmapHeight = "-1";
     selectable = "1";
     autoResize = "1";
  };
};
