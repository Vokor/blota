exec("./Blota_Dashboard.gui");
exec("./iconHandler.cs");

function Blota_Dashboard::selectPane(%this, %id)
{
    Blota_Dashboard_paneArrow.position = getWord(("Blota_Dashboard_paneIcon" @ %id).position, 0) + 4 SPC "0";
    Blota_Dashboard_pane0.setVisible(false);
    Blota_Dashboard_pane1.setVisible(false);
    Blota_Dashboard_pane2.setVisible(false);
    ("Blota_Dashboard_pane" @ %id).setVisible(true);
}

function BDash_Open()
{
    canvas.PushDialog(Blota_Dashboard);
    blota_ToggleGameMenu(0);
    blota_toggleOptMenu(0);
    Blota_DT_IconAreaBG.setVisible(0);
    Blota_DT_IconArea_RTB.setVisible(0);
    Blota_DT_IconArea_Blockland.setVisible(0);
    Blota_DT_IconArea_Options.setVisible(0);
    Blota_DT_ExitAreaBG.setVisible(0);
    Blota_DT_ExitButton.setVisible(0);
    Blota_DT_IconArea_Player.setVisible(0);
    MM_AuthBar.setVisible(0);
}

function BDash_Close()
{
    canvas.PopDialog(Blota_Dashboard);
    Blota_DT_IconAreaBG.setVisible(1);
    Blota_DT_IconArea_RTB.setVisible(1);
    Blota_DT_IconArea_Blockland.setVisible(1);
    Blota_DT_IconArea_Options.setVisible(1);
    Blota_DT_ExitAreaBG.setVisible(1);
    Blota_DT_ExitButton.setVisible(1);
    Blota_DT_IconArea_Player.setVisible(1);
    MM_AuthBar.setVisible(1);
}