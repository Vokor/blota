// Blota_Dashboard
//  Author: Fluffy

function BDash_AddIcon(%iconName, %imageFile, %text, %command)
{
    %icon = new GuiSwatchCtrl(%iconName) {
      profile = "GuiDefaultProfile";
      horizSizing = "right";
      vertSizing = "bottom";
      position = "0 0";
      extent = "150 150";
      minExtent = "8 2";
      enabled = "1";
      visible = "1";
      clipToParent = "1";
      color = "0 0 0 0";
      BIconText = %text;
      BIconCommand = %command;

      new GuiTextCtrl() {
         profile = "BlotaBodyTextC";
         horizSizing = "center";
         vertSizing = "bottom";
         position = "2 130";
         extent = "145 18";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         text = %text;
         maxLength = "255";
      };
      new GuiBitmapCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "center";
         vertSizing = "bottom";
         position = "11 2";
         extent = "128 128";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         bitmap = %imageFile;
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
      };
   };
    
    // Calculate positioning
    %maxX = mFloor(getWord(BDash_IconCanvas.getExtent(), 0) / 150) - 1;
    %maxY = mFloor(getWord(BDash_IconCanvas.getExtent(), 1) / 150) - 1;
    
    %iconCount = BDash_IconCanvas.getCount();
    
    while(true)
    {
        %safe++;
        if(%safe > 500)
            break;
        
        if(%column > %maxX)
        {
            %column = 0;
            %row ++;
        }
        
        if(%row > %maxY)
        {
            return;
        }
        
        %pos = %column * 150 SPC %row * 150;
        echo(%pos);
        %column++;
        if(isObject(getIconFromPos(%pos)))
        {
            continue;
        }
        else
        {
            echo("success");
            %iconPos = %pos;
            break;
        }
    }
    
    %icon.position = %iconPos;
    
    BDash_IconCanvas.add(%icon);
}

// ----------------------------------------------------------------------------------------------
//  App Positioning Handler (Taken from BlockOS, but that's ok because I made the flaming thing!
// ----------------------------------------------------------------------------------------------

// Needs to: onMouseClick->GetPositionOfMouse->FindWhatIcon->onMouseDrag->MoveIcon->SaveIconPos
package BDashIconHandler
{
    // Detects when the left mouse button is pushed down
    function GuiMouseEventCtrl::onMouseDown(%this, %mod, %pos, %click)
    {
        // Check if it's the BlockOS control
        if(%this.getName() $= "BDash_IconOverlay")
        {
            // Get the icon the mouse clicked on
            %obj = getIconFromPos(%pos);
            if(isObject(%obj))
            {
                // Store the icon object
                BDash_IconOverlay.selectedIcon = %obj;
                BDash_IconOverlay.selectedIcon.originalPos = %obj.position;
                BDash_IconOverlay.selectedIconOffset = getWord(%pos, 0) - getWord(%obj.position, 0) SPC getWord(%pos, 1) - getWord(%obj.position, 1);
            }
        }
        // Make sure we don't break everything else by parenting the function
        parent::onMouseDown(%this, %mod, %pos, %click);
    }
    
    // This function is called when a user drags the mouse
    function GuiMouseEventCtrl::onMouseDragged(%this, %bool, %pos, %x)
    {
        // Check if it's the BlockOS control
        if(%this.getName() $= "BDash_IconOverlay")
        {
            // Find what icon the user clicked on in the previous function
            %obj = BDash_IconOverlay.selectedIcon;
            
            // Some maths that put icon at a set distace away from the mouse
            %x = getWord(%pos, 0) - getWord(BDash_IconOverlay.selectedIconOffset, 0);
            %y = getWord(%pos, 1) - getWord(BDash_IconOverlay.selectedIconOffset, 1);
            
            // Moves the icon as the mouse moves
            %obj.position = %x SPC %y;
        }
        // Make sure we don't break everything else by parenting the function
        parent::onMouseDragged(%this, %bool, %pos, %x);
    }

    // This function is called when the user let's go of the left mouse button
    function GuiMouseEventCtrl::onMouseUp(%this, %mod, %pos, %click)
    {
        // Again, to check if it's the BlockOS control
        if(%this.getName() $= "BDash_IconOverlay")
        {
            // Now, if the user has not dragged the icon
            if(BDash_IconOverlay.selectedIcon.position $= BDash_IconOverlay.selectedIcon.originalPos)
            {
                // We run the icon's command
                eval(BDash_IconOverlay.selectedIcon.BIconCommand);
                BDash_IconOverlay.selectedIcon = "";
            }
            else
            {
                // If the user has dragged the icon, we run a check to see if it's in a valid place
                schedule(100, 0, BDash_IconValidation, BDash_IconOverlay.selectedIcon);
                // We then save these to the desktop.dat file
                //schedule(500, 0, BlockOS_SaveIconPositions);
            }
            // Clears the selected icon from the control
        }
        // Make sure we don't break everything else by parenting the function
        parent::onMouseUp(%this, %mod, %pos, %click);
    }
};
// Start the mouse functions
activatePackage(BDashIconHandler);

// This is the function we use to check if the icon is in a valid position
function BDash_IconValidation(%iconObject)
{
    // Get the x and y positions of the icon
    %x = getWord(%iconObject.position, 0);
    %y = getWord(%iconObject.position, 1);
    
    // This simply snaps the icons to the 90px grid grid
    %x = mRound(%x / 150) * 150;
    %y = mRound(%y / 150) * 150;
    
    if(isObject(getIconFromPos(%x SPC %y)) || %x < 0 || %y < 0 || %x > (getWord(BDash_IconCanvas.getExtent(), 0) - 150) || %y > (getWord(BDash_IconCanvas.getExtent(), 1) - 150))
    {
        %isSorted = true;
        %iconObject.position = BDash_IconOverlay.selectedIcon.originalPos;
    }
    
    // if the icon is not sorted by any of the previous code
    if(!%isSorted)
    {
        // Put it where the user requested
        %iconObject.position = %x SPC %y;
    }
    BDash_IconOverlay.selectedIcon = "";
}

function BDash_LoadIconPositions(%icon)
{
    %fo = new FileObject();
    %fo.openForRead("config/client/blota/dashIconArray.dat");
    
    while(!%fo.isEOF())
    {
        %line = %fo.readLine();
        %object = getField(%line, 0);
        
        if(strLen(%object) < 1)
            continue;
            
        %object.position = getField(%line, 1);
    }
    
    %fo.close();
    %fo.delete();
}

function BDash_SaveIconPositions(%icon)
{
    for(%i = 0; %i < BDash_IconCanvas.getCount(); %i++)
    {
        %object = BDash_IconCanvas.getObject(%i);
        
        %temp = %object.getName() TAB %object.position;
        %lineData = %lineData NL %temp;
    }
    %fo = new FileObject();
    %fo.openForWrite("config/client/blota/dashIconArray.dat");
    
    %fo.writeLine(%lineData);
    
    %fo.close();
    %fo.delete();
}

function getIconFromPos(%pos)
{
    %mouseX = getWord(%pos, 0);
    %mouseY = getWord(%pos, 1);
    
    for(%i = 0; %i < BDash_IconCanvas.getCount(); %i++)
    {
        %obj = BDash_IconCanvas.getObject(%i);
        %iconXpos = getWord(%obj.position, 0);
        %iconYpos = getWord(%obj.position, 1);
        
        %iconXDimension = getWord(%obj.extent, 0);
        %iconYDimension = getWord(%obj.extent, 1);
        
        if(%mouseX >= %iconXpos && %mouseY >= %iconYpos && %mouseX < %iconXpos + %iconXDimension && %mouseY < %iconYpos + %iconYDimension)
        {
            return %obj;
        }
    }
    return false;
}

function mRound(%a)
{
    if(%a - mFloor(%a) > 0.5) 
        return mCeil(%a); 
    else 
        return mFloor(%a);
}