// Image Downloader (Wallpaper Previews)

NewServerGui_flushImgCache("config/client/blota/wallCache/*.png");

function BDash_LoadWallpaperPreviews(%path, %ids)
{
    if(!isObject(BDashWallTCP))
    {
        new TCPObject(BDashWallTCP)
        {
            path = %path;
            ids = %ids;
        };
    }
    BDashWallTCP.fetchPreviews();
    BDashWallTCP.progress = 0;
}

function BDashWallTCP::fetchPreviews(%this)
{
    %this.currID = getField(%this.ids, %this.progress);
    if(%this.currID $= "")
    {
        %this.progress = 0;
        BDash_RenderWallIcons();
    }
    %this.getPath = %this.path @ %this.currID @ ".png";
    
    %packet = "";
    %packet = %packet @ "GET " @ %this.getPath @ " HTTP/1.0\r\n";
    %packet = %packet @ "Host: datafling.com\r\n";
    %packet = %packet @ "User-Agent: Torque/1.0\r\n\r\n";
    
    %this.packet = %packet;
    %this.connect("datafling.com:80");
}

function BDashWallTCP::onConnected(%this)
{
    %this.send(%this.packet);
}

function BDashWallTCP::onLine(%this)
{
    if(getWord(%line, 0) $= "Content-Length:")
    {
        %this.length = getword(%line, 1);
    }
    if(%line $= "")
    {
        %this.setBinarySize(%this.length);
    }
}

function BDashWallTCP::onBinChunk(%this, %chunk)
{
    if(%chunk >= %this.length) 
    {
            %this.saveBufferToFile("config/client/blota/wallCache/" @ %this.currID @ "_prev.png");
            %this.disconnect();
            %this.setBinary(false);
    }
}

function BDashWallTCP::onDisconnect(%this)
{
    %this.BDash_WallPapers_addPrevIcon("config/client/blota/wallCache/" @ %this.currID @ "_prev.png", %this.progress);
    %this.progress++;
    %this.fetchPreviews();
}

// Preview handler

function BDashWallTCP::BDash_WallPapers_addPrevIcon(%this, %img, %id)
{
    %pane = Blota_Dashboard_WallScroll;
    
    %obj = new GuiBitmapButtonCtrl("WallIcon_" @ %id) {
               profile = "GuiDefaultProfile";
               horizSizing = "right";
               vertSizing = "bottom";
               position = "6 12";
               extent = "250 150";
               minExtent = "8 2";
               enabled = "1";
               visible = "1";
               clipToParent = "1";
               command = "BDash_SelectWallpaper(" @ %this.currID @ ");";
               groupNum = "-1";
               buttonType = "PushButton";
               bitmap = "config/client/blota/wallCache/" @ %this.currID @ "_prev.png";
               lockAspectRatio = "0";
               alignLeft = "0";
               overflowImage = "0";
               mKeepCached = "0";
               mColor = "255 255 255 255";
               text = "";
    };
    
    // Position handling
    
    %maxX = 3;
    %pane.col++;
    
    if(getWord(getRes(), 0) < 900)
        %maxX = 2;
    
    if(%id % %maxX == 0)
    {
        %pane.row++;
        %pane.col = 0;
    }
    
    %pos = (%pane.col * 255) SPC (%pane.row * 155);
    %obj.pos = %pos;
    %pane.add(%obj);
}