// Fonts
echo("loading fonts");
if(!isFile("base/client/ui/cache/Open Sans Light_36.gft"))
    fileCopy("./Open Sans Light_36.gft", "base/client/ui/cache/Open Sans Light_36.gft");
    
if(!isFile("base/client/ui/cache/Open Sans_16.gft"))
    fileCopy("./Open Sans_16.gft", "base/client/ui/cache/Open Sans_16.gft");
    
if(!isFile("base/client/ui/cache/Open Sans_20.gft"))
    fileCopy("./Open Sans_20.gft", "base/client/ui/cache/Open Sans_20.gft");
    
if(!isFile("base/client/ui/cache/Open Sans_40.gft"))
    fileCopy("./Open Sans_40.gft", "base/client/ui/cache/Open Sans_40.gft");