// +==================================================
//  Handling Code for new server browser
// +==================================================


// Load GUI Element
ManualJoin.delete();
exec("./NewServerGuiv2.gui");
exec("./ManualJoin.gui");


$blota::newServerList::currID = 0;
$blota::newServerGui::downloadpreviews = 1;
newServerGui_searchDropDown.add("Host\'s Name", 0);
newServerGui_searchDropDown.add("Server Name", 1);
newServerGui_searchDropDown.add("Gamemode", 2);

// GUI Handling

function newServerGui_JoinGame()
{
    %id = $blota::newServerList::selectedServer;
    if(strLen(%id) < 1)
    {
        messageBoxOk("Error", "You must first select a server!");
        return;
    }
    
    %ip = ("ServerImg_" @ $blota::newServerList::selectedServer).IP;
    %port = ("ServerImg_" @ $blota::newServerList::selectedServer).port;
    
    if(("ServerImg_" @ $blota::newServerList::selectedServer).passworded $= "Yes")
    {
        canvas.pushDialog(ManualJoin);
        MJ_txtIP.setValue(%ip @ ":" @ %port);
        MJ_txtIP.enabled = false;
        return;
    }
    
    MJ_txtIP.setValue(%ip @ ":" @ %port);
    MJ_connect();
}

function newServerGui_manualConnect()
{
    MJ_txtIP.enabled = true;
    MJ_txtIP.setValue("");
    canvas.pushDialog(ManualJoin);
}

function newServerGui_Search()
{
    %through = newServerGui_searchDropDown.getValue();
    %for = newServerGui_searchInput.getValue();
    
    switch$(%through)
    {
        case "Host\'s Name":
            %through = "host";
        
        case "Server Name":
            %through = "name";
            
        case "Gamemode":
            %through = "gamemode";
    }
    
    for(%i = 0; %i < NewServerGui_ListCanvas.getCount(); %i++)
    {
        %obj = NewServerGui_ListCanvas.getObject(%i);
        
        if(strPos(strupr(%obj.bvar[%through]), strupr(%for)) < 0)
        {
            %obj.delete();
            %i--;
        }
    }
    
    newServerGui_fixPositions("search");
}

function newServerGui_SortByID()
{
    %x = 0;
    for(%i = 0; %i < $blota::newServerList::currID; %i++)
    {
        %obj = ("Base_" @ %i);
        
        if(!isObject(%obj))
            continue;
            
        %obj.position = "0" SPC %x * 31;
        %x++;
    }
}

function newServerGui_fixPositions(%type, %id)
{
    for(%i = 0; %i < NewServerGui_ListCanvas.getCount(); %i++)
    {
        if(%type $= "search")
        {
            %obj = NewServerGui_ListCanvas.getObject(%i);
            %obj.position = 0 SPC %i * 31;
        }
        
        if(%type $= "open")
        {
            %obj = NewServerGui_ListCanvas.getObject(%i);
            
            if(%obj.bID <= %id)
                continue;
            
            %obj.position = getWord(%obj.position, 0) SPC (getWord(%obj.position, 1) + 131);
        }
        
        if(%type $= "close")
        {
            %obj = NewServerGui_ListCanvas.getObject(%i);
            
            if(%obj.bID <= %id)
                continue;
            
            %obj.position = getWord(%obj.position, 0) SPC (getWord(%obj.position, 1) - 131);
        }
    }
    
    if(%type $= "search")
    {
        NewServerGui_ListCanvas.resize(getWord(NewServerGui_ListCanvas.position, 0), getWord(NewServerGui_ListCanvas.position, 1), "278", NewServerGui_ListCanvas.getCount() * 31);
        newServerGui_SortByID();
    }
    
    if(%type $= "open")
    {
        NewServerGui_ListCanvas.resize(getWord(NewServerGui_ListCanvas.position, 0), getWord(NewServerGui_ListCanvas.position, 1), "278", getWord(NewServerGui_ListCanvas.extent, 1) + 131);
    }
    
    if(%type $= "close")
    {
        NewServerGui_ListCanvas.resize(getWord(NewServerGui_ListCanvas.position, 0), getWord(NewServerGui_ListCanvas.position, 1), "278", getWord(NewServerGui_ListCanvas.extent, 1) - 131);
    }
}

function newServerGui_joinID(%id)
{
    $blota::newServerList::selectedServer = %id;
    newServerGui_JoinGame();
}

function newServerGui_toggleServerOpen(%id)
{
    %obj = ("Base_" @ %id);
    
    if(!isObject(%obj))
        return;
    
    if(%obj.closed)
    {
        $blota::newServerList::selectedServer = %id;
        %obj.extent = "277 161";
        %obj.setBitmap("Add-Ons/system_blota/images/ui/servListBGOpen");
        %obj.closed = false;
        
        newServerGui_fixPositions("open", %id);
        
        if($blota::newServerGui::downloadpreviews && ($sim::time - %obj.lastUpdate) > 60)
        {
            %obj.lastUpdate = $sim::time;
            NewServerGui_StopAnyDownloads();
            newServerGui_NextImage(%id);
        }
        if(($sim::time - %obj.lastUpdate) < 60 && $blota::newServerGui::downloadpreviews)
        {
            %objI = ("ServerImg_" @ %id);
                
            if(!isObject(%objI))
                return;
                    
            if(!isFile("config/client/blota/imgCache/" @ %id @ ".jpg"))
                return;
                    
            %objI.setBitmap("config/client/blota/imgCache/" @ %id @ ".jpg");
        }
    }
    else
    {
        %obj.extent = "277 30";
        %obj.setBitmap("Add-Ons/system_blota/images/ui/servListBGClosed");
        %obj.closed = true;
        newServerGui_fixPositions("close", %id);
    }
}

function newServerGui_AddServerToList(%ip, %port, %name, %players, %maxPlayers, %host, %brickCount, %gamemode, %dedicated, %passworded)
{
    //Create ctrls
    %id = $blota::newServerList::currID;
    %pos = 0 SPC NewServerGui_ListCanvas.getCount() * 31;
    
    %ctrl = new GuiBitmapCtrl("Base_" @ $blota::newServerList::currID) {
      profile = "GuiDefaultProfile";
      horizSizing = "right";
      vertSizing = "bottom";
      position = "131 115";
      extent = "277 30";
      minExtent = "8 2";
      enabled = "1";
      visible = "1";
      clipToParent = "1";
      bitmap = "Add-Ons/system_blota/images/ui/servListBGClosed";
      wrap = "0";
      lockAspectRatio = "0";
      alignLeft = "0";
      overflowImage = "0";
      keepCached = "0";
      mColor = "255 255 255 255";
      mMultiply = "0";
    bvar["host"] = %host;
    bvar["name"] = %name;
    bvar["gamemode"] = %gamemode;
    closed = true;
    lastUpdate = -60;
    bID = $blota::newServerList::currID;

      new GuiMLTextCtrl() {
         profile = "BlotaHeaderText";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "7 6";
         extent = "210 18";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         lineSpacing = "2";
         allowColorChars = "0";
         maxChars = "-1";
         text = getSubStr(%name, 0, 32);
         maxBitmapHeight = "-1";
         selectable = "1";
         autoResize = "1";
      };
      new GuiBitmapCtrl("KeyIcon_" @ $blota::newServerList::currID) {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "202 8";
         extent = "16 16";
         minExtent = "8 2";
         enabled = "1";
         visible = "0";
         clipToParent = "1";
         bitmap = "Add-Ons/system_blota/images/ui/key";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
      };
      new GuiMLTextCtrl() {
         profile = "BlotaHeaderText";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "223 6";
         extent = "48 18";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         lineSpacing = "2";
         allowColorChars = "0";
         maxChars = "-1";
         text = %players @ "/" @ %maxPlayers;
         maxBitmapHeight = "-1";
         selectable = "1";
         autoResize = "1";
      };
      new GuiBitmapCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "142 41";
         extent = "16 16";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         bitmap = "Add-Ons/system_blota/images/ui/user";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
      };
      new GuiBitmapCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "142 71";
         extent = "16 16";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         bitmap = "Add-Ons/system_blota/images/ui/brick";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
      };
      new GuiBitmapCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "142 101";
         extent = "16 16";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         bitmap = "Add-Ons/system_blota/images/ui/server";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
      };
      new GuiBitmapCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "142 131";
         extent = "16 16";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         bitmap = "Add-Ons/system_blota/images/ui/controller";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
      };
      new GuiMLTextCtrl() {
         profile = "BlotaBodyText";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "170 43";
         extent = "203 12";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         lineSpacing = "2";
         allowColorChars = "0";
         maxChars = "-1";
         text = %host;
         maxBitmapHeight = "-1";
         selectable = "1";
         autoResize = "1";
      };
      new GuiMLTextCtrl() {
         profile = "BlotaBodyText";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "170 73";
         extent = "203 12";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         lineSpacing = "2";
         allowColorChars = "0";
         maxChars = "-1";
         text = %brickCount;
         maxBitmapHeight = "-1";
         selectable = "1";
         autoResize = "1";
      };
      new GuiMLTextCtrl() {
         profile = "BlotaBodyText";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "170 103";
         extent = "203 12";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         lineSpacing = "2";
         allowColorChars = "0";
         maxChars = "-1";
         text = %dedicated;
         maxBitmapHeight = "-1";
         selectable = "1";
         autoResize = "1";
      };
      new GuiMLTextCtrl() {
         profile = "BlotaBodyText";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "170 133";
         extent = "203 12";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         lineSpacing = "2";
         allowColorChars = "0";
         maxChars = "-1";
         text = %gamemode;
         maxBitmapHeight = "-1";
         selectable = "1";
         autoResize = "1";
      };
      new GuiBitmapButtonCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "0 0";
         extent = "277 30";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         command = "newServerGui_toggleServerOpen(" @ $blota::newServerList::currID @ ");";
         groupNum = "-1";
         buttonType = "PushButton";
         bitmap = "Add-Ons/system_blota/images/ui/trans";
         lockAspectRatio = "0";
         text = "";
         alignLeft = "0";
         overflowImage = "0";
         mKeepCached = "0";
         mColor = "255 255 255 255";
      };
      new GuiBitmapCtrl("ServerImg_" @ $blota::newServerList::currID) {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "2 31";
         extent = "128 128";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         bitmap = "Add-Ons/system_blota/images/ui/loadingImg";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
         IP = %ip;
         port = %port;
         passworded = %passworded;
      };
      
        new GuiBitmapButtonCtrl() {
         profile = "GuiDefaultProfile";
         horizSizing = "right";
         vertSizing = "bottom";
         position = "244 131";
         extent = "16 16";
         minExtent = "8 2";
         enabled = "1";
         visible = "1";
         clipToParent = "1";
         text = "";
         bitmap = "Add-Ons/system_blota/images/ui/door";
         wrap = "0";
         lockAspectRatio = "0";
         alignLeft = "0";
         overflowImage = "0";
         keepCached = "0";
         mColor = "255 255 255 255";
         mMultiply = "0";
         command = "newServerGui_joinID(" @ $blota::newServerList::currID @ ");";
      };
   };
   
   if(%passworded $= "Yes")
   {
       ("KeyIcon_" @ $blota::newServerList::currID).setVisible(1);
   }
   
    $blota::newServerList::currID++;
    
    %ctrl.position = %pos;      
    NewServerGui_ListCanvas.resize(getWord(%pos, 0), getWord(%pos, 1), "278", (NewServerGui_ListCanvas.getCount() + 1) * 31);
    NewServerGui_ListCanvas.add(%ctrl);
}

function newServerGui_flushImgCache(%path)
{
    for (%i = findFirstFile(%path); strLen(%i) > 0; %i = findNextFile(%path)) 
    {
        fileDelete(%i);
    }
}

// New Master server handler because the default is protected.
function newServerGui_QueryMaster()
{
    if(isObject(newMasterQuery))
        newMasterQuery.delete();
        
    NewServGui_Loaderer.setVisible(1);
    NewServerGui_ListCanvas.delete();
    
    new GuiSwatchCtrl(NewServerGui_ListCanvas) {
       profile = "GuiDefaultProfile";
       horizSizing = "right";
       vertSizing = "bottom";
       position = "1 1";
       extent = "278 2";
       minExtent = "8 2";
       enabled = "1";
       visible = "1";
       clipToParent = "1";
       color = "0 0 0 0";
    };
    NewServerGui_Scroll.add(NewServerGui_ListCanvas);
    $blota::newServerList::currID = 0;
    $blota::newServerList::selectedServer = "";
    $blota::newServerList::lastRefresh = $sim::time;
    
    new HTTPObject(newMasterQuery).get("master2.blockland.us:80", "/index.php");
}

function newMasterQuery::onLine(%this, %line)
{
    if(%line $= "START")
    {
        %this.blotaReady = true;
        return;
    }
    
    if(!%this.blotaReady)
        return;
        
    if(strLen(%line) < 1)
        return;
        
    if(%line $= "END")
    {
        NewServGui_Loaderer.schedule(300, setVisible, 0);
        %this.blotaReady = false;
        return;
    }
    %host = getField(strReplace(getField(%line, 4), "\'" , "\t"), 0);
    newServerGui_AddServerToList(getField(%line, 0), getField(%line, 1), getField(%line, 4), getField(%line, 5), getField(%line, 6), %host, getField(%line, 8), getField(%line, 7), (getField(%line, 3) ? "Yes" : "No"), (getField(%line, 2) ? "Yes" : "No"));
}

function newMasterQuery::onDisconnect(%this)
{
    %this.schedule(100, delete);
}

// Image downloaderator

function newServerGui_fetchServerPreview(%ip, %port, %id)
{
    if(isObject(newServerGui_ImgPrevTCP))
        newServerGui_ImgPrevTCP.delete();
    
    %ip = strReplace(%ip, ".", "-");
    %port = %port;
    
    %data = %ip @ "_" @ %port;
    
    %packet = %packet @ "GET /thumb.php?q=" @ %data @ " HTTP/1.1\r\n";
    %packet = %packet @ "Host: image.blockland.us\r\n";
    %packet = %packet @ "\r\n";
    
    %tcp = new TCPObject(newServerGui_ImgPrevTCP)
    {
        packet = %packet;
        imgID = %id;
    };
    
    %tcp.connect("image.blockland.us:80");
}

function newServerGui_ImgPrevTCP::onConnected(%this)
{
    %this.send(%this.packet);
}

function newServerGui_ImgPrevTCP::onLine(%this, %line)
{
    if(getWord(%line, 0) $= "Content-Length:")
        %this.length = getWord(%line, 1);
    
    if(%line $= "")
    {
        if(%this.length <= "225")
        {
            if(isObject(("ServerImg_" @ %this.imgID)))
                ("ServerImg_" @ %this.imgID).setBitmap("add-ons/system_blota/images/ui/blackFill.png");
                
            newServerGui_stopAnyDownloads();
            return;
        }
            
        %this.setBinarySize(%this.length);
    }
}

function newServerGui_ImgPrevTCP::onBinChunk(%this, %chunk)
{
    if(%chunk >= %this.length)
    {
        %this.saveBufferToFile("config/client/blota/imgCache/" @ %this.imgID @ ".jpg");
        
        if(isObject(("ServerImg_" @ %this.imgID)))
            ("ServerImg_" @ %this.imgID).setBitmap("config/client/blota/imgCache/" @ %this.imgID @ ".jpg");
        
        %this.disconnect();
    }
}

function newServerGui_ImgPrevTCP::onDisconnect(%this)
{
    %this.delete();
}

function newServerGui_NextImage(%id)
{
    if(!isObject("ServerImg_" @ %id))
    {
        return;
    }
    %ip = ("ServerImg_" @ %id).IP;
    %port = ("ServerImg_" @ %id).port;
    newServerGui_fetchServerPreview(%ip, %port, %id);
}

function newServerGui_stopAnyDownloads()
{
    if(isObject(newServerGui_ImgPrevTCP))
    {
        newServerGui_ImgPrevTCP.disconnect();
        newServerGui_ImgPrevTCP.delete();
    }
}

function NewServerGui::onWake(%this)
{
    if($pref::Gui::AutoQueryMasterServer)
        newServerGui_QueryMaster();
}

NewServerGui_flushImgCache("config/client/blota/imgCache/*.jpg");