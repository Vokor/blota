// Blota
//  Profiles Module

new GuiControlProfile(BlotaWindowProfile)
{
   opaque = true;
   border = 0;
   fillColor = "255 255 255 255";
   text = "GuiWindowCtrl test";
   bitmap = "Add-ons/System_Blota/images/ui/window.png";
   hasBitmapArray = true;
};

new GuiControlProfile(BlotaGlassWindowProfile)
{
   opaque = true;
   border = 0;
   justify = "center";
   fillColor = "255 255 255 12";
   text = "GuiWindowCtrl test";
   fontColor = "255 255 255 255";
   fontSize = 12;
   fontType = "Verdana";
   bitmap = "Add-ons/System_Blota/images/dashboard/glassWindowArray.png";
   hasBitmapArray = true;
};

new GuiControlProfile(BlotaGlassCheckBox)
{
   opaque = false;
   fillColor = "232 232 232";
   border = false;
   borderColor = "0 0 0";
   fontColor = "255 255 255 255";
   fontSize = 12;
   fontType = "Verdana";
   fontColorHL = "32 100 100";
   fixedExtent = true;
   justify = "left";
   bitmap = "Add-ons/System_Blota/images/dashboard/glassCheck.png";
   hasBitmapArray = true;
};

new GuiControlProfile(BlotaScrollProfile)
{
   hasBitmapArray = true;
   bitmap = "Add-ons/System_Blota/images/ui/scrollProfile.png";
};

new GuiControlProfile(BlotaCheckBox)
{
   opaque = false;
   fillColor = "232 232 232";
   border = false;
   borderColor = "0 0 0";
   fontColor = "120 120 120 255";
   fontSize = 12;
   fontType = "Verdana";
   fontColorHL = "32 100 100";
   fixedExtent = true;
   justify = "left";
   bitmap = "Add-ons/System_Blota/images/ui/torqueCheck.png";
   hasBitmapArray = true;
};

new GuiControlProfile(BlotaHeaderText)
{
   fontColor = "30 30 30 255";
   fontSize = 18;
   fontType = "Impact";
   justify = "Left";  
};

new GuiControlProfile(BlotaHoverText)
{
   fontColor = "200 200 200 255";
   fontSize = 18;
   fontType = "Impact";
   justify = "Left";  
};

new GuiControlProfile(BlotaBodyText)
{
   fontColor = "50 50 50 255";
   fontSize = 12;
   fontType = "Verdana";
   justify = "Left";
};

new GuiControlProfile(BlotaBodyTextC)
{
   fontColor = "255 255 255 255";
   fontSize = 16;
   fontType = "Open Sans";
   justify = "center";
};

new GuiControlProfile(BlotaLoadingTextC)
{
   fontColor = "25 25 25 255";
   fontSize = 16;
   fontType = "Open Sans";
   justify = "center";
};

new GuiControlProfile(BlotaDropDown)
{
    autoSizeHeight = false;
    autoSizeWidth = false;
    border = true;
    borderColor = "50 50 50 255";
    borderThickness = "1";
    hasBitmapArray = true;
    bitmap = "Add-ons/System_Blota/images/ui/scrollProfile.png";
    fillColor = "220 220 220 255";
    fillColorHL = "220 220 220 255";
    fillColorNA = "220 220 220 255";
    modal = true;
    opaque = true;
    
    fontColor = "50 50 50 255";
    fontSize = 12;
    fontType = "Verdana";
    justify = "Center";
};

new GuiControlProfile(BlotaInput) {
   tab = "1";
   canKeyFocus = "1";
   mouseOverSelected = "0";
   modal = "1";
   opaque = "0";
   fillColor = "255 255 255 100";
   fillColorHL = "128 128 128 255";
   fillColorNA = "200 200 200 255";
   border = "0";
   borderThickness = "1";
   borderColor = "100 100 100 255";
   borderColorHL = "128 128 128 255";
   borderColorNA = "64 64 64 255";
   fontType = "Verdana";
   fontSize = "12";
   fontColors[0] = "120 120 120 255";
   fontColors[1] = "255 255 255 255";
   fontColors[2] = "128 128 128 255";
   fontColors[3] = "200 200 200 255";
   fontColors[4] = "255 96 96 255";
   fontColors[5] = "0 0 255 255";
   fontColors[6] = "0 0 0 0";
   fontColors[7] = "0 0 0 0";
   fontColors[8] = "0 0 0 0";
   fontColors[9] = "0 0 0 0";
   fontColor = "0 0 0 255";
   fontColorHL = "255 255 255 255";
   fontColorNA = "128 128 128 255";
   fontColorSEL = "200 200 200 255";
   fontColorLink = "255 96 96 255";
   fontColorLinkHL = "0 0 255 255";
   doFontOutline = "0";
   fontOutlineColor = "255 255 255 255";
   justify = "left";
   textOffset = "0 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
};

new GuiControlProfile(BlotaGlassInput) {
   tab = "1";
   canKeyFocus = "1";
   mouseOverSelected = "0";
   modal = "1";
   opaque = "1";
   fillColor = "255 255 255 60";
   fillColorHL = "128 128 128 255";
   fillColorNA = "200 200 200 255";
   border = "1";
   borderThickness = "1";
   borderColor = "100 100 100 255";
   borderColorHL = "128 128 128 255";
   borderColorNA = "64 64 64 255";
   fontType = "Verdana";
   fontSize = "12";
   fontColors[0] = "255 255 255 255";
   fontColors[1] = "255 255 255 255";
   fontColors[2] = "255 255 255 255";
   fontColors[3] = "255 255 255 255";
   fontColors[4] = "255 255 255 255";
   fontColors[5] = "255 255 255 255";
   fontColors[6] = "0 0 0 0";
   fontColors[7] = "0 0 0 0";
   fontColors[8] = "0 0 0 0";
   fontColors[9] = "0 0 0 0";
   fontColor = "255 255 255 255";
   fontColorHL = "255 255 255 255";
   fontColorNA = "128 128 128 255";
   fontColorSEL = "200 200 200 255";
   fontColorLink = "255 96 96 255";
   fontColorLinkHL = "0 0 255 255";
   doFontOutline = "0";
   fontOutlineColor = "255 255 255 255";
   justify = "left";
   textOffset = "0 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
};

new GuiControlProfile(BlotaSliderProfile) {
   tab = "0";
   canKeyFocus = "0";
   mouseOverSelected = "0";
   modal = "1";
   opaque = "0";
   fillColor = "242 241 240 255";
   fillColorHL = "228 228 235 255";
   fillColorNA = "255 255 255 255";
   fillColorSEL = "98 100 137 255";
   border = "0";
   borderThickness = "1";
   borderColor = "100 100 100 255";
   borderColorHL = "50 50 50 50";
   borderColorNA = "75 75 75 255";
   bevelColorHL = "255 0 255 255";
   bevelColorLL = "255 0 255 255";
   fontType = "Arial";
   fontSize = "14";
   fontCharset = "ANSI";
   fontColors[0] = "0 0 0 255";
   fontColors[1] = "0 0 0 255";
   fontColors[2] = "0 0 0 255";
   fontColors[3] = "255 255 255 255";
   fontColors[4] = "255 0 255 255";
   fontColors[5] = "255 0 255 255";
   fontColors[6] = "255 0 255 255";
   fontColors[7] = "255 0 255 255";
   fontColors[8] = "255 0 255 255";
   fontColors[9] = "255 0 255 255";
   fontColor = "0 0 0 255";
   fontColorHL = "0 0 0 255";
   fontColorNA = "0 0 0 255";
   fontColorSEL = "255 255 255 255";
   fontColorLink = "255 0 255 255";
   fontColorLinkHL = "255 0 255 255";
   justify = "Left";
   textOffset = "0 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
   bitmap = "Add-ons/System_Blota/images/ui/slider";
   hasBitmapArray = "0";
   category = "Core";
   canSave = "1";
   canSaveDynamicFields = "1";
};