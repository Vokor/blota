// Options
if(isObject(OptionsDlg))
{
    OptionsDlg.delete();
}
exec("./optionsDlg.gui");

if(isObject(JoinServerGui))
{
    JoinServerGui.delete();
}
exec("./JoinServerGui.gui");

if(isObject(ManualJoin))
{
    ManualJoin.delete();
}
exec("./ManualJoin.gui");

package Blota_Replacements
{
    function joinServerGui::onWake(%this)
    {
        parent::onWake(%this);
        
        %this.queryWebMaster();
    }
    
    function JS_serverList::addRow(%this, %id, %row)
    {
        %passworded = getField(%row, 0);
        %dedicated  = getField(%row, 1);
        %name       = getField(%row, 2);
        %ping       = getField(%row, 3);
        %players    = getField(%row, 4);
        %dash       = getField(%row, 5);
        %maxPlayers = getField(%row, 6);
        %brickCount = getField(%row, 7);
        %gamemode   = getField(%row, 8);

        if(%dedicated $= "Yes")
        {
            %dedicated = "Dedi";
        }
        else
        {
            %dedicated = "";
        }
        
        if(stripMLControlChars(%passworded) $= "Yes")
        {
            %passworded = "Pass";
        }
        else
        {
            %passworded = "";
        }
        
        parent::addRow(%this, %id, %passworded TAB %dedicated TAB %name TAB %ping TAB %players TAB %dash TAB %maxPlayers TAB %brickCount TAB %gamemode);
    }
};
activatePackage(Blota_Replacements);

//exec("./newServerGui/main.cs");