function Blota_Desktop_refresh()
{
	Blota_DT_Background.setbitmap($Blota::Wallpaper);
}

function Blota_Desktop_timeLoop()
{
	cancel($Blota::TimeLoop);
	
	%time = getSubStr(getDateTime(),9,5);
	%len = strLen(%time);
	if($Blota::TopBar::Hours)
	{
		%m = " AM";
		%firstColon = strPos(%time,":");
		%hours = getSubStr(%time,0,%firstcolon);
		if(%hours > 12)
		{
			%hours = %hours - 12;
			%m = " PM";
			%time = %hours @ getSubStr(%time,%firstColon,%len);
		}
	}
	%date = blota_getDate();
	Blota_DT_TopBar_LeftText.setText("<shadow:2:2><shadowcolor:00000066><color:e0e0c1><font:segoe ui bold:15>  " SPC %time @ %m SPC "<font:calibri:15>  " @ %date);

	//Other stuff
	if($blota::topbarIcons < 0)
	{
		$Blota::TopBarIcons = 0;
	}
	else if($Blota::topBarIcons > 3)
	{
		$blota::topBarIcons = 0;
	}
	
	//Check if menu buttons are present, then remove them, if so.
	if(MainMenuButtonsGui.isAwake() && $Blota::Desktop)
	{
	    canvas.popDialog(MainMenuButtonsGui);
	}
	$Blota::TimeLoop = schedule(500, 0, Blota_Desktop_timeLoop);
}


function blota_Desktop_Rightloop()
{
	cancel($Blota::RightLoop);
	$Blota::RightLoop = schedule(5000, 0, Blota_Desktop_RightLoop);
	%num1 = getWord(blota_getRTBNumber(), 0);
	%num2 = getWord(blota_getRTBNumber(), 1);
	//%pic = blota_getRTBStatusPic();
	blota_setRtbStatusPic();
	Blota_DT_TopBar_RightText.setText("<shadow:2:2><shadowcolor:00000066><color:e0e0c1><just:right><font:calibri:15>" @ " " @ %num2 @ " playing, " @ %num1 @ " online" @ "   ");
	if(lagIcon.visible == 1){
		$Blota::TopBarIcons -= 1;
		lagIcon.setVisible(1);
	}
	if(HUD_SuperShift.visible == 1){
		$Blota::TopBarIcons -= 1;
		HUD_SuperShift.setVisible(1);
	}
}
function blota_getRTBNumber()
{
	%offlineGroup = RTBCC_Roster.getGroupByName("Offline Users");
	%offlineCount = %offlineGroup.getCount();
	//echo("offline user count = " @ %offlineCount);
	%totalUsers = RTBCC_Roster.getUsercount();
	//echo("total user count = " @ %totalUsers);
	%onLineCount = %totalUsers - %offlineCount;
	//echo("online user count & total - offline = " @ %onLineCount);

	%onlineCount3 = 0;
	%playingCount = 0;
	%this = RTBCC_Roster.getGroupByName("Friends");
	if(!isObject(%this))
	{
		%ret = %onlineCount3 TAB %playingCount;
		return %ret;
	}

   for(%i=0;%i<%this.getCount();%i++)
   {
      %user = %this.getObject(%i);
      //echo("%user (user|online|status) = " @ %user @ "|" @ %user.online @ "|" @ %user.status);
      if(%user.online)
      {
      	%onlineCount3++;
      	if(%user.status > 0)
      	{
      		%playingCount++;
      	}
      }
   }
  //echo("%onlineCount3 = " @ %onlineCount3);
   //echo("%playingCount = " @ %playingCount);
   %ret = %onlineCount3 TAB %playingCount;
   return %ret;
}

function blota_setRtbStatusPic()
{
	%text = RTBCC_StatusIcon.bitMap;
	if(striPos(%text, "Offline") > 0)
	{
		%ret = $Blota::ImagePath @ "circle_offline";
	}
	else if(striPos(%text, "Online") > 0)
	{
		%ret = $Blota::ImagePath @ "circle_online";
	}
	else if(striPos(%text, "Away") > 0)
	{
		%ret = $Blota::ImagePath @ "circle_away";
	}
	else if(striPos(%text, "Busy") > 0)
	{
		%ret = $Blota::ImagePath @ "circle_do-not-disturb";
	}
	else
	{
		return "You broke it.";
	}
	%ret2 = blota_getRTBNumber();
	%online = getWord(%ret2, 0);
	%playing = getWord(%ret2, 1);
	if(%playing >= 10)
	{
		%x = getWord(getRes(),0) - 140;
	}
	else if(%playing < 10 && %online >= 10)
	{
		%x = getWord(getRes(),0) - 135;
	}
	else if(%online < 10)
	{
		%x = getWord(getRes(),0) - 130;
	}
	Blota_DT_TopBar_StatusIcon.setbitmap(%ret);
	//%x = $blota::Width - (2 + (%len * 3.7));
	Blota_DT_TopBar_StatusIcon.resize(%x, 5, 14, 14);
}

function blota_getDate()
{
	//07/23/12
	%date = getDateTime();
	%monthNum = getSubStr(%date,0,2);
	switch(%monthNum)
	{
		case 00:
			%month = "lol u broke it";
		case 01:
			%month = "January";
		case 02:
			%month = "Feburary";
		case 03:
			%month = "March";
		case 04:
			%month = "April";
		case 05:
			%month = "May";
		case 06:
			%month = "June";
		case 07:
			%month = "July";
		case 08:
			%month = "August";
		case 09:
			%month = "September";
		case 10:
			%month = "October";
		case 11:
			%month = "November";
		case 12:
			%month = "December";
	}
	%preday = getSubStr(%date, 3, 2);
	if(%preday < 10)
	{
		%day = getSubStr(%date, 4, 1);
	}
	if(%preday >= 10)
	{
		%day = %preday;
	}
	%year = getSubStr(%date, 6, 2);
	%text = %month SPC %day @ "," SPC "20" @ %year;
	return %text;
}

function blota_changeRTBStatus()
{
	%text = RTBCC_StatusIcon.bitMap;
	if(striPos(%text, "Offline") > 0)
	{
		RTB_ConnectClient.setStatus("Online",1);
	}
	else if(striPos(%text, "Online") > 0)
	{
		RTB_ConnectClient.setStatus("Away",1);
	}
	else if(striPos(%text, "Away") > 0)
	{
		RTB_ConnectClient.setStatus("Busy",1);
	}
	else if(striPos(%text, "Busy") > 0)
	{
		RTB_ConnectClient.setStatus("Offline",1);
	}
	blota_Desktop_rightloop();
}

function blota_ToggleGameMenu(%bool)
{
	if(%bool)
	{
		if($Blota::GameMenu)
		{
			Blota_DT_gameMenuBG.setVisible(0);
			Blota_DT_gameMenu_start.setVisible(0);
			Blota_DT_gameMenu_join.setVisible(0);
			$Blota::GameMenu = 0;
		}
		else if(!$Blota::GameMenu)
		{
			Blota_DT_gameMenuBG.setVisible(1);
			Blota_DT_gameMenu_start.setVisible(1);
			Blota_DT_gameMenu_join.setVisible(1);
			$Blota::GameMenu = 1;
		}
	}
}

function blota_ToggleOptMenu(%bool)
{
	if(%bool)
	{
		if($Blota::optionsMenu)
		{
			Blota_DT_optionsMenuBG.setVisible(0);
			Blota_DT_optionsMenu_blockland.setVisible(0);
			Blota_DT_optionsMenu_blota.setVisible(0);
			Blota_DT_Version_Blota.setVisible(1);
			Blota_DT_Version_rtb.setVisible(1);
			Blota_DT_Version_bl.setVisible(1);
			$Blota::optionsMenu = 0;
		}
		else if(!$Blota::optionsMenu)
		{
			Blota_DT_optionsMenuBG.setVisible(1);
			Blota_DT_optionsMenu_blockland.setVisible(1);
			Blota_DT_optionsMenu_blota.setVisible(1);
            Blota_DT_Version_Blota.setVisible(0);
			Blota_DT_Version_rtb.setVisible(0);
			Blota_DT_Version_bl.setVisible(0);
			$Blota::optionsMenu = 1;
		}
	}
}

function blota_tempBlotaOpt()
{
	clientCmdMessageBoxOK("Blota Options...","The blota settings are not created yet, and will be by beta 2. The problem is, I don't know what options we need, please go to the <a:goo.gl/r4ye7>form</a> and suggest what options should be made!"); 
}