//Notifications

//Animation Manager
//This code is tooken from RTB, thanks Ephi!

schedule(500, 0, Blota_createAnimMngr);
function Blota_createAnimMngr()
{
	if(isObject(Blota_NotificationManager))
	{
		Blota_NotificationManager.destroy();
		Blota_NotificationManager.delete();
	}
   	echo("Blota: Blota_notificationManager Created");
	new ScriptGroup(Blota_NotificationManager);
}

function clientCmdBlota_MakeNotification(%title, %message, %icon, %time, %id, %size, %sound)
{
	blota_notificationManager.makeNotification(%title, %message, %icon, %time, %id, %size, %sound);
}

function Blota_NotificationManager::makeNotification(%this, %title, %message, %icon, %Time, %id, %size, %sound, %rtb)
{
	if(%id !$= "")
	{
		for(%i=0;%i<%this.getCount();%i++)
		{
			%notification = %this.getObject(%i);
			if(%notification.id $= %id)
			{
				%notification.icon = %icon;
				%notification.title = %title;
				%notification.message = %message;
				%notification.render();
				return;
			}
		}
	}

	if(!%rtb)
	{
		%rtb = 0;
	}

	if(%size $= "")
	{
		%size = "large";
	}
	//$blota::imagePath@"notificons/"@
	//$RTB::Path@"images/icons/"@
	if(%icon $= "")
	{
		%icon = $blota::imagePath@"notificons/blue";
	}
	//else if(striPos(%id,"joinleave_") != -1 || striPos(%id,"_msg") != -1 || striPos(%id,"_friend") != -1 || striPos(%id,"_invite") != -1 ||  striPos(%id,"join_") != -1 || striPos(%id,"_sJoin") != -1 || striPos(%id,"_information") != -1 || striPos(%id,"_RTBOpen") != -1)
	//{
	//	%icon = $RTB::Path@"images/icons/" @ %icon;
	//}
	else
	{
		%icon = $blota::imagePath@"notificons/"@%icon;
	}


	if(striPos(%id,"joinleave_") != -1 || striPos(%id,"_msg") != -1 || striPos(%id,"_friend") != -1 || striPos(%id,"_invite") != -1 ||  striPos(%id,"join_") != -1 || striPos(%id,"_sJoin") != -1 || striPos(%id,"_information") != -1 || striPos(%id,"_RTBOpen") != -1)
	{
		%rtb = 1;
	}
	else
	{
		%rtb = 0;
	}


	if(%time $= "")
	{
		%time = 2500;
	}
	else if(%time == -1)
	{
		%time = $Blota::StickyTime;
	}

	%notif= new ScriptObject()
	{
		class = "Blota_Notification";
      
		id = %ID;
		icon = %icon;
		title = %title;
		message = %message;
		holdTime = %Time;
		size = %size;
		rtb = %rtb;
   };

   %this.add(%notif);
   %notif.render();

   if(%sound)
   {
   	schedule(700, 0, alxPlay, Blota_notifSound);
   }
}

function Blota_NotificationManager::finishNotification(%this,%id)
{
	for(%i=0;%i<%this.getCount();%i++)
	{
		%notif = %this.getObject(%i);
		if(%notif.id $= %id)
		{
			%notification.state = "up";
			%notification.Move();
			break;
		}
   }
}

//function Blota_NotificationManager::refocus(%this)
//{
//   for(%i=0;%i<%this.getCount();%i++)
//   {
//      %notification = %this.getObject(%i);
//      if(isObject(%notification.canvas) && %notification.canvas.script $= %notification)
//        %canvas = blota_getCanvasObject2();
//        %canvas.add(%notification.canvas);
//   }
//}

function Blota_NotificationManager::destroy(%this)
{
   for(%i=0;%i<%this.getCount();%i++)
   {
      %notification = %this.getObject(%i);
      if(isObject(%notification.canvas) && %notification.canvas.script $= %notification)
      {
         cancel(%notification.moveAnim);
         %notification.canvas.delete();
      }
   }
   %this.clear();
}

function Blota_NotificationManager::destroyRTBNotifs(%this)
{
	for(%i=0;%i<%this.getCount();%i++)
   {
      %notification = %this.getObject(%i);
      if(isObject(%notification.canvas) && %notification.canvas.script $= %notification && %notification.rtb)
      {
         cancel(%notification.moveAnim);
         %notification.canvas.delete();
      }
   }
   %this.clear();
}

function Blota_Notification::render(%this)
{
	if(%this.size $= "large")
	{
		%width = 272;
		%height = 112;
	}
	else if(%this.size $= "small")
	{
		%width = 272;
		%height = 42;
	}
   
   if(isObject(%this.canvas) && %this.canvas.script $= %this)
   {
      //%this.setIcon(%this.icon);
      %this.setTitle(%this.title);
      %this.setMessage(%this.message);
      
      cancel(%this.moveAnim);
      //%this.state = "down";
      
      //%this.step();
   }
   else
   {
   		switch($blota::notifCorner)
   		{
   			case 0:
   				%xposition = 0;
   				%ySmallMove = 40;
   				%yLargeMove = 112;
   				%yposition = 30;

   			case 1:
   				%xPosition = getWord(getRes(),0) - %width;
   				%ySmallMove = 40;
   				%yLargeMove = 112;
   				%yPosition = 30;

   			case 2:
   				%xPosition = 0;
   				%ySmallMove = -40;
   				%yLargeMove = -112;
   				%yPosition = getWord(getRes(),1) - %height;

   			case 3:
   				%xPosition = getWord(getRes(),0) - %width;
   				%ySmallMove = -40;
   				%yLargeMove = -112;
   				%yPosition = getWord(getRes(),1) - %height;
   		}
      //%xPosition = getWord(getRes(),0) - %width;
      //%yPosition = 30;
      %equalY = 0;
      %manager = %this.getGroup();

      for(%i = 0; %i < %manager.getCount(); %i++)
      {
         %notification = %manager.getObject(%i);
         //echo("Render: notification " @ %i @ " canvas position = " @ %notification.canvas.position);
         if(isObject(%notification.canvas))// && %notification.canvas.script $= %notification)
         {
         	//echo("render: notification " @ %i @ " made first if");
            if(getWord(%notification.canvas.position,1) >= %yPosition)
            {
          	 	//echo("render: notification " @ %i @ " made second if");
				if(%notification.size $= "small")
          	 	{
          	 		%yposition = getWord(getWord(%notification.canvas.position, 1) + %ySmallMove, 0); //%ySmallMove = 40
          	 	}
          	 	else if(%notification.size $= "large")
          	 	{
          	 		%yposition = getWord(getWord(%notification.canvas.position, 1) + %yLargeMove, 0); //%yLargeMove = 112
          	 	}
               //%yPosition = getWord(getWord(%notification.canvas.extent,1) + 25,0); //getWord(%notification.canvas.position,1)-%height
               //echo("render: notification " @ %i @ " yposition now = " @ %yposition);
            }
            else
            {
            	//echo("render: notification " @ %i @ " didn't make second if");
            }
         }
         else
         {
         	//echo("render: notification " @ %i @ " didn't make first if");
         }
      }
      
      if(%yPosition < 25)
         return;
      
      %canvas = new GuiSwatchCtrl()
      {
         position = %xPosition SPC %yPosition;
         extent = %width SPC %height;
         color = "0 0 0 0";
         
         script = %this;
      };
      //if(Canvas.getObject(canvas.getCount()-1).getName() $= "ConsoleDlg")
      //{
      //	if(Canvas.getObject(canvas.getCount()-2).getName() $= "NewChatHud")
      //	{
      //		if(Canvas.getObject(canvas.getCount()-3).getName() $= "RTB_Overlay")
      //		{
      //			canvas.getObject(Canvas.getCount()-4).add(%canvas);
      //			//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-4).getName());
      //		}
      //		else
      //		{
      //			canvas.getObject(Canvas.getCount()-3).add(%canvas);
      //			//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-3).getName());
      //		}
      //	}
      //	else
      //	{
      //		if(Canvas.getObject(canvas.getCount()-2).getName() $= "RTB_Overlay")
      //		{
      //			Canvas.getObject(canvas.getCount()-3).add(%canvas);
      //			//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-3).getName());
      //		}
      //		else
      //		{
      //			Canvas.getObject(canvas.getCount()-2).add(%canvas);
      //			//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-2).getName());
      //		}
      //	}
      //}
      //else if(Canvas.getObject(canvas.getCount()-1).getName() $= "NewChatHud")
      //{
      //	if(Canvas.getObject(canvas.getCount()-2).getName() $= "ConsoleDlg")
      //	{
      //		if(Canvas.getObject(canvas.getCount()-3).getName() $= "RTB_Overlay")
      //		{
      //			canvas.getObject(Canvas.getCount()-4).add(%canvas);
      //			//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-4).getName());
      //		}
      //		else
      //		{
      //			canvas.getObject(Canvas.getCount()-3).add(%canvas);
      //			//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-3).getName());
      //		}
      //	}
      //	else
      //	{
      //		Canvas.getObject(canvas.getCount()-2).add(%canvas);
      //		//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-2).getName());
      //	}
      //}
      //else
      //{
      //	canvas.getObject(canvas.getCount()-1).add(%canvas);
      //	//echo("Blota: Adding Notif to Canvas: " @ canvas.getObject(Canvas.getCount()-1).getName());
      //}
     //echo("BLOTA: DB: lastCanvas = " @ Canvas.getObject(canvas.getCount()-1).getName());
      //Canvas.getObject(canvas.getCount()-1).add(%canvas);
      //canvas.add(%canvas);
      %canvas1 = blota_getCanvasObject2(); //Try two
      %canvas1.add(%canvas);
      
      %this.canvas = %canvas;
      
      if(canvas.getContent().getName() $= "PlayGui")
         %this.drawPlay();
      else
         %this.drawMenu();
         
      //%this.step();
   }
}

function blota_getCanvasObject()
{
	%d = -1;
	%a = -1;

	while((%d * -1) < canvas.getCount())
	{
		if(canvas.getObject(canvas.getCount()+%d).getName() $= "ConsoleDlg" || canvas.getObject(canvas.getCount()+%d).getName() $= "NewChatHud" || canvas.getObject(canvas.getCount()+%d).getName() $= "RTB_Overlay")
		{
			%a = %a - 1;
		}
		%d = %d-1; 
	}
	return canvas.getObject(canvas.getCount()+%a);	
}

function blota_getCanvasObject2() //try two
{
	%canvas = "";
	%b = 0;
	%a = 1;

	while(!%b)
	{
		%canvas = canvas.getObject(canvas.getCount()-%a).getName();
		if(%canvas $= "mainMenuGui" || %canvas $= "loadingGui" || %canvas $= "playGui" || %canvas $= "RTB_Overlay")
		{
			%b = true;
			%canv = canvas.getObject(canvas.getCount()-%a);
			return %canv;
		}
		else if(%a > 25)
		{
			echo("BLOTA ERROR: blota_getCanvasObject2");
			return "ERROR";
		}
		else
		{
			%a++;
		}
	}
}


function Blota_Notification::step(%this)
{
   if(%this.state $= "down")
   {
   	//echo(%this.window.position);
      if(getWord(%this.window.position,1) >= 0)
      {
      	//echo("DONE" SPC %this.window.position);
         if(%this.holdTime < 0)
         {
            %this.window.position = "0 0";
            %this.state = "done";
            return;
         }
         %this.window.position = "0 0";
         %this.state = "wait";
         %this.moveAnim = %this.schedule(%this.holdTime,"step");
         return;
      }
      %this.window.position = vectorAdd(%this.window.position,"0 1");
      %this.moveAnim = %this.schedule(1,"step");
   }
   else if(%this.state $= "wait")
   {
      %this.state = "up";
      %this.step();
   }
   else if(%this.state $= "up")
   {
   		//echo(%this.window.position);
      if(getWord(%this.window.position,1) <= (getWord(%this.canvas.extent,1)-(getWord(%this.window.extent,1)*2)))
      {
      	//echo("DONE" SPC %this.window.position);
         %this.window.position = getWord(%this.canvas.extent,0) SPC "0";
         %this.state = "done";
         %this.step();
         return;
      }
      %this.window.position = vectorSub(%this.window.position,"0 1");
      %this.moveAnim = %this.schedule(1,"step");
   }
   else if(%this.state $= "done")
   {
   	//echo("ELSE IF STATE DOWN");
      %y = getWord(%this.canvas.position,1);
      %this.canvas.delete();

      for(%i=0;%i<Blota_NotificationManager.getCount();%i++)
      {
         %notification = Blota_NotificationManager.getObject(%i);
         if(%notification == %this)
         {
         	return;
         }

         if(!isObject(%notification.canvas))
         {
         	return;
         }

         if(getWord(%notification.canvas.position,1) $= "25")
         {
         	//don't shift
         }
         else if(%i == 1)
         {
         	if(%this.size $= "large")
         	{
         		%notification.canvas.shift(0,-96);
         	}
         	else if(%this.size $= "small")
         	{
         		%notification.canvas.shift(0,-25);
         	}
         }
         else
         {
         	if(%lastNotif.size $= "large")
         	{
         		%notification.canvas.shift(0,-96);
         	}
         	else if(%lastNotif.size $= "small")
         	{
         		%notification.canvas.shift(0,-25);
         	}
         }
         %lastNotif = %notification;
         //if(!isObject(%notification.canvas))
         //{
         //	echo("NO CANVAS DUMBO");
         //   continue;
         //}
         //if(getWord(%notification.canvas.position,1) < %y)
         //{
         //	echo("SHIFTING");
         //   %notification.canvas.shift(0,-106);
         //}
      }
      %this.delete();
   }
}

function Blota_Notification::drawPlay(%this)
{
	%this.drawMenu();
}

function Blota_Notification::drawMenu(%this)
{
	if(%this.size $= "large")
	{
   %draw = new GuiFadeinBitmapCtrl() {
      position = "0" SPC "0";
      extent = "272 112";
      bitmap = $Blota::ImagePath@"largenew";
      waitTime = %this.holdTime;
      fadeinTime = "500";
      fadeoutTime = "500";
  };

    %draw2 = new GuiSwatchCtrl() {
      position = "0 0";
      extent = "272 112";
      color = "0 0 0 0";
      
      new GuiBitmapCtrl() {
         position = "20 13";
         extent = "20 20";
         bitmap = %this.icon;
      };
      new GuiMLTextCtrl() {
         position = "25 10";
         extent = "241 19"; 
         text = "<shadow:2:2><shadowcolor:00000066><color:e0e0c1><font:segoe ui bold:20><just:right>"@%this.title@"  ";
         selectable = false;
      };
      new GuiMLTextCtrl() {
         position = "19 39";
         extent = "234 30";
         text = "<shadow:2:2><shadowcolor:00000066><color:e0e0c1><font:calibri:15>"@%this.message;
         selectable = false;
      };
      	new GuiBitmapButtonCtrl() {
         position = "0 0";
         extent = "272 112";
         text = " ";
         command = "Blota_NotifButton(" @ %this @ ");"; //RTB_Overlay.fadeIn();
      };
   };
	}
	else if(%this.size $= "small")
	{
		 %draw = new GuiFadeInBitmapCtrl() {
      position = "0" SPC "0";
      extent = "272 42";
      bitmap = $Blota::ImagePath@"smallnew";
      waitTime = %this.holdTime;
      fadeinTime = "500";
      fadeoutTime = "500";
      notif = %this;
  		};

  	%draw2 = new GuiSwatchCtrl() {
      position = "0 0";
      extent = "272 42";
      color = "0 0 0 0";
      
      new GuiBitmapCtrl() {
         position = "15 11";
         extent = "20 20";
         bitmap = %this.icon;
      };
      new GuiMLTextCtrl() {
         position = "42 12";
         extent = "234 30";
         text = "<shadow:2:2><shadowcolor:00000066><color:e0e0c1><font:calibri:15>"@%this.message;
         selectable = false;
      };
      	new GuiBitmapButtonCtrl() {
         position = "0 0";
         extent = "272 42";
         text = " ";
         command = "Blota_NotifButton(" @ %this @ ");"; //RTB_Overlay.fadeIn();
      };
   };
	}
   %this.canvas.add(%draw);
   %this.canvas.schedule(500, add, %draw2);

   %this.window = %draw;
   %this.window2 = %draw2;

   if(%this.holdTime > 0)
   {
   		%time = (%draw.fadeInTime + %draw.waittime) + (%draw.fadeOutTime - 300);
   		%draw2.schedule(%time, delete);
   		%draw.schedule(%time + 300, delete);
   		%this.canvas.schedule(%time + 300, delete);
   }
}

function Blota_Notification::setIcon(%this,%icon)
{
   %this.window2.getObject(0).setBitmap(%icon);
}

function Blota_Notification::setTitle(%this,%title)
{
   %this.window2.getObject(1).setText("<shadow:2:2><shadowcolor:00000066><color:e0e0c1><font:segoe ui bold:20><just:right>"@%title@"  ");
}

function Blota_Notification::setMessage(%this,%message)
{
   %this.window2.getObject(2).setText("<shadow:2:2><shadowcolor:00000066><color:e0e0c1><font:calibri:15>"@%message);
}

function blota_notifButton(%this)
{
	if(%this.rtb)
	{
		RTB_Overlay.fadeIn();
	}
}