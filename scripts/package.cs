//Package
package Blota_DeskTop
{
	function RTBCC_Socket::onConnected(%this)
	{
		parent::onConnected(%this);
		schedule(5000, 0, Blota_Desktop_rightLoop);
		Blota_DT_TopBar_StatusIcon.schedule(5000, setvisible, 1);
	}

	function playGui::onwake(%this,%obj)
	{
		playGui.add(Blota_TopBar);
		parent::onWake(%this,%obj);
		lagIcon.setVisible(0);
		$Blota::TopBarIcons = 0;
	}

	function RTB_Overlay::onwake(%this,%obj)
	{
		RTB_Overlay.add(Blota_TopBar);
		parent::onWake(%this,%obj);
	}

	function RTB_Overlay::OnSleep(%this)
	{
		if(playGui.isAwake())
		{
			playGui.add(Blota_TopBar);
		}
		else if(mainMenuGui.isAwake())
		{
			mainMenuGui.add(Blota_TopBar);
		}
		else if(LoadingGui.isAwake())
		{
			loadingGui.add(Blota_TopBar);
		}
		parent::onSleep(%this);
	}

	function mainMenuGui::onwake(%this,%obj)
	{
		if(!Blota_TopBar.isAwake())
		{
			mainMenuGUI.add(Blota_TopBar);
		}
		$blota::topBarIcons = 0;
		//canvas.popDialog(MM_Authbar);
		//schedule(5000, 0, blota_addAuthBar);
		//Blota_Desktop.add(MM_AuthBar);
		//MM_AuthBar.setBitmap($Blota::ImagePath @ "authBar.png");
		//trace(1);
		//blota_addAuthBar();
		//parent::onWake(%this,%obj);
	}

	function Blota_Options::onWake(%this,%obj)
	{
		//parent::onWake(%this,%obj);
		if($Blota::NoSaveNotifPosAdd $= "")
   		{
   			Blota_Opt_BOpt_Scroll_NotifPos.add("Top Left", 0);
   			Blota_Opt_BOpt_Scroll_NotifPos.add("Top Right", 1);
   			Blota_Opt_BOpt_Scroll_NotifPos.add("Bottom Left", 2);
   			Blota_Opt_BOpt_Scroll_NotifPos.add("Bottom Right", 3);
   			$BlotaNoSave::NotifPosAdd = 1;
   		}
   		Blota_Opt_BOpt_Scroll_NotifPos.setSelected($Blota::RtbNotif);
    	setmodpaths(getmodpaths());
    
    	for (%i = 0;%i < Blota_Opt_Bg_ScrollSwatch.getCount();%i++) 
    	{
       		Blota_Opt_Bg_ScrollSwatch.getObject(%i).delete();
    	}
    
    	Blota_Opt_Bg_ScrollSwatch.clear();
    
    	%path = "config/client/blota/walls/*.png";
    
    	for(%i = findFirstFile(%path);strLen(%i) > 0;%i = findNextFile(%path)) 
    	{
       		%ctrl = new GuiBitmapCtrl() 
        	{
            	incrementsTotalHeight = true;
            	bitmap = getSubStr(%i, 0, strLen(%i) - 4);
            	lockAspectRatio = 1;
            	extent = 280 SPC 125;
            	position = 5 SPC ((Blota_Opt_Bg_ScrollSwatch.getCount() / 2) * 130) + 5;
        	};
        
        	%overlay = new GuiBitmapButtonCtrl() {
            	incrementsTotalHeight = true;
            	extent = %ctrl.extent;
            	position = %ctrl.position;
            	command = "Blota_Background_Use(" @ %ctrl.getID() @ ");";
        	};

        	Blota_Opt_Bg_ScrollSwatch.add(%ctrl);
        	Blota_Opt_Bg_ScrollSwatch.add(%overlay);
    	}

		%path = "config/client/blota/walls/*.jpg";

    	for(%i = findFirstFile(%path);strLen(%i) > 0;%i = findNextFile(%path)) 
    	{
       		%ctrl = new GuiBitmapCtrl() 
        	{
            	incrementsTotalHeight = true;
            	bitmap = getSubStr(%i, 0, strLen(%i) - 4);
            	lockAspectRatio = 1;
            	extent = 280 SPC 125;
            	position = 5 SPC ((Blota_Opt_Bg_ScrollSwatch.getCount() / 2) * 130) + 5;
        	};
        
        	%overlay = new GuiBitmapButtonCtrl() {
            	incrementsTotalHeight = true;
            	extent = %ctrl.extent;
            	position = %ctrl.position;
            	command = "Blota_Background_Use(" @ %ctrl.getID() @ ");";
        	};

        	Blota_Opt_Bg_ScrollSwatch.add(%ctrl);
        	Blota_Opt_Bg_ScrollSwatch.add(%overlay);
    	}
    	%last = Blota_Opt_Bg_ScrollSwatch.getObject(Blota_Opt_Bg_ScrollSwatch.getCount() - 1);
    
    	Blota_Opt_Bg_ScrollSwatch.extent = 300 SPC getWord(%last.position, 1) + getWord(%last.extent, 1);
    }

	function Blota_Background_Use(%ctrl)
	{
   		Blota_DT_Background.setBitmap(%ctrl.bitmap);
   		$Blota::Wallpaper = %ctrl.bitmap;
	}

	function blota_addAuthBar()
	{
		MM_AuthBar.setBitmap($Blota::ImagePath @ "authBar.png");
		oldMainMenu.remove(MM_Authbar);
		mainMenuGUI.add(MM_Authbar);
		for(%i = 0; %i < mainMenuGui.getcount(); %i++)
		{
			%dlg = mainMenuGui.getObject(%i);
			//echo("DLG TEXT = " @ %dlg.text);
			if(strPos(%dlg.text,"RTB Version:") >= 0)
			{
				mainMenuGui.remove(%dlg);
			}
		}
	}

	function blota_authBarBitmap()
	{
		MM_AuthBar.setBitmap($Blota::ImagePath @ "authBar.png");
	}

	function MM_AuthBar::blinkSuccess(%this)
	{
		if($blota::Desktop)
		{
			Blota_addAuthBar();
			schedule(300, 0, blota_authBarBitmap);
		}
		parent::blinkSuccess(%this);
		//MM_AuthBar.setBitmap($Blota::ImagePath @ "authBar.png");
	}

	function LoadingGui::onwake(%this,%obj)
	{
		$Blota::TopBarIcons = 0;
		if(!Blota_TopBar.isAwake() && $Blota::TopBar::Ingame)
		{
			LoadingGui.add(Blota_TopBar);
		}
		LOAD_MapPicture.setBitmap($Blota::Wallpaper);
		parent::onWake(%this,%obj);
	}

	function chatWhosTalkingText::onWake(%this,%obj)
	{
		if($Blota::Topbar::InGame)
		{
			//%this.setVisible(0);
			%extent = %this.extent;
			%this.resize(0,getWord(newChatText.position,1)-15,getWord(%extent,0),getWord(%extent,1));
			//%this.resize(0,getWord(newChatText.position,1)-13,getWord(%extent,0),getWord(%extent,1));
		}
		parent::chatWhosTalkingText(%this,%obj);
	}

	function newChatText::onWake(%this,%obj)
	{
		if($Blota::Topbar::InGame)
		{
			%extent = %this.extent;
			%this.resize(2,42,getWord(%extent,0),getWord(%extent,1));
		}
		parent::onWake(%this,%obj);
	}

	function Hud_EnergyBar::onWake(%this,%obj)
	{
		if($Blota::Topbar::InGame)
		{
			%extent = %this.extent;
			%this.resize(getWord(%this.position, 0),35,getWord(%extent,0),getWord(%extent,1));
		}
		parent::onWake(%this,%obj);
	}

	function Hud_HealthBar::onWake(%this,%obj)
	{

		parent::onWake(%this,%obj);
		if($Blota::Topbar::InGame)
		{
			%extent = %this.extent;
			%this.resize(getWord(%this.position, 0),35+9+3,getWord(%extent,0),getWord(%extent,1));
		}
	}

	function playGUI::hideToolBox(%q, %obj, %b, %a)
	{
		parent::hideToolBox(%q, %obj, %b, %a);
		if($Blota::Topbar::InGame)
		{
			%this = Hud_ToolBox;
			%extent = %this.extent;
			%position = %this.position;
			if(getWord(%position, 1) $= "0")
			{
				%this.resize(getWord(%position, 0),getWord(%position, 1)+25,getWord(%extent,0),getWord(%extent,1));
			}
			%this = Hud_ToolNameBG;
			%extent = %this.extent;
			if(getWord(%positon, 1) $= "0" || getWord(%position, 1) $= "320")
			{
				%this.resize(getWord(%position, 0),getWord(%position, 1)+25,getWord(%extent,0),getWord(%extent,1));
			}
		}
	}

	function optionsDlg::onSleep(%this)
	{
		parent::onSleep(%this);
		//playGui.add(Blota_Desktop);
		//mainMenuGui.add(Blota_Desktop);
		if(mainMenuGui.isAwake())
		{
			Blota_DT_IconArea_Options.setVisible(0);
			Blota_DT_IconArea_Options.setVisible(1);
		}
	}


	function canvas::popDialog(%this, %dlg)
	{
		//if(%dlg.getName() $= "optionsDlg")
		//{
		//	mainMenuGUI.add(Blota_Desktop);
		//}
		//Blota_NotificationManager.refocus();
		parent::popDialog(%this,%dlg);
	}

	function canvas::pushDialog(%this, %dlg)
	{
	    if(%dlg.getName() $= "MainMenuButtonsGui")
	        return;
	        
		parent::pushDialog(%this,%dlg);
		//canvas.pushDialog(blota_topbar);

        if(%dlg $= "RTB_Overlay")
        	Blota_NotificationManager.destroyRTBNotifs();
	}

	function Blota_DT_IconArea_Options::onMouseEnter(%this,%a,%b)
	{
		alxPlay(Blota_HoverSound);
	}

	function Blota_DT_IconArea_Player::onMouseEnter(%this,%a,%b)
	{
		alxPlay(Blota_HoverSound);
	}

	function Blota_DT_IconArea_RTB::onMouseEnter(%this,%a,%b)
	{
		alxPlay(Blota_HoverSound);
	}

	function Blota_DT_IconArea_Blockland::onMouseEnter(%this,%a,%b)
	{
		alxPlay(Blota_HoverSound);
	}

	function Blota_DT_ExitButton::onMouseEnter(%this,%a,%b)
	{
		alxPlay(Blota_HoverSound);
	}

	//Notification stuff, thanks to RTB again!
  

   function blota_Options::onSleep(%this)
   {
   		switch$(Blota_Opt_BOpt_Scroll_NotifPos.getValue())
   		{
   			case "Top Left":
   				$Blota::RtbNotif = 0;

   			case "Top Right":
   				$Blota::RtbNotif = 1;

   			case "Bottom Left":
   				$Blota::RtbNotif = 2;

   			case "Botton Right":
   				$Blota::RtbNotif = 3;
   		}
   		export("$Blota::*","config/client/blota/prefs.cs");
   		Blota_DT_TopBarBG.setBitMap(blota_getTopBarTrans());
   		//$blota::RtbNotif = Blota_Opt_BOpt_Scroll_NotifPos.getValue();
   }

   function RTBCC_NotificationManager::push(%this,%title,%message,%icon,%key,%holdTime)
   {
   		if($blota::RTBNotif)
   		{
   			if(%title $= "Connected" && %message $= "You are now online.")
   			{
   				%key = "rtb_connected";
   				%message = "Successfully connected to RTB Connect.";
   				%title = "";
   				%icon = "green";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(striPos(%key,"join_") != -1)
   			{
   				%message = "You have joined " @ %title @ ".";
   				%icon = "green";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(striPos(%key,"_friend") != -1)
   			{
   				%message = %title @ " wants to be your friend!";
   				%icon = "yellow";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(striPos(%key,"_msg") != -1)
   			{
   				//Do nothing, messages are handled differently.
   			}
   			else if(striPos(%key,"joinLeave_") != -1)
   			{
   				%icon = "green";
   				if(striPos(%message, "joined.") != -1)
   				{
   					%pos = strPos(%message," joined.");
   					%person = getsubStr(%message,0,%pos);
   					%message = %person @ " joined " @ %title @ ".";
   				}
   				else if(striPos(%message, "left.") != -1)
   				{
   					%pos = strPos(%message," left.");
   					%person = getsubStr(%message,0,%pos);
   					%message = %person @ " left " @ %title @ ".";
   				}

   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(striPos(%key,"_invite") != -1)
   			{
   				%message = %title @ " has invited you to play.";
   				%icon = "yellow";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(%message $= "has just joined a server.")
   			{
   				%key = %key @ "_sjoin";
   				%message = %title @ " has just joined a server.";
   				%title = "";
   				%icon = "green";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(%message $= "has just started a server.")
   			{
   				%key = %key @ "_sjoin";
   				%message = %title @ " has just joined a server.";
   				%title = "";
   				%icon = "green";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}
   			else if(%message $= "has just signed in.")
   			{
   				%key = %key @ "_sjoin";
   				%message = %title @ " has just signed in.";
   				%title = "";
   				%icon = "green";
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"small");
   			}	
   			else
   			{
   				Blota_notificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"large");
   			}
   		}
   		else
   		{
   			parent::push(%this,%title,%message,%key,%holdtime);
   		}
   }

   function RTBCC_Session::receive(%this,%message)
   {
   	   	parent::receive(%this,%message);
   		if($blota::rtbNotif)
   		{
   			if(RTBCO_getPref("CC::Message::Note"))
   			{
   				if(!RTB_Overlay.isAwake())
   				{
   					%key = %this.user.id @ "_msg";
   					%holdTime = -1;
   					%title = %this.user.name @ " says";
   					%message = %message.find("body").cData;
   					%icon = "blue";
   					Blota_NotificationManager.makeNotification(%title,%message,%icon,%holdtime,%key,"large");
   				}
   			}
   		}
   }

   //Top bar Icons: lagIcon, superShiftIcon, isTyping
	function lagIcon::setVisible(%this, %bool)
	{
		if($Blota::Topbar::InGame)
		{
        	if(%bool == 0)
        	{
           		$Blota::TopBarIcons -= 1;
				if(HUD_SuperShift.visible == 1)
				{
					//$Blota::TopBarIcons -= 1;
					HUD_SuperShift.schedule(1,setVisible,1);
				}
				if(Blota_RTBIsTyping.visible == 1)
				{
					//$Blota::TopBarIcons -= 1;
					Blota_RTBIsTyping.schedule(1,setVisible,1);
				}
            	//echo($Blota::TopBarIcons);
        	}
        	else if(%bool == 1)
        	{
            	$Blota::TopBarIcons += 1;
				//echo($Blota::TopBarIcons);
				lagIcon.setBitmap($Blota::ImagePath @ "topBarIcons/lag.png");
				%xpos = getWord(Blota_DT_TopBar_StatusIcon.position, 0);
				%prex = 27 * $Blota::TopBarIcons;
				%x = %xpos - %prex; //Ish, how much bigger the icon is
				lagIcon.resize(%x, 2, 20, 20);
				Blota_TopBar.add(lagIcon);
        	}
        }
        parent::setVisible(%this, %bool);
    }
	function HUD_SuperShift::setVisible(%this, %bool)
    {
    	if($Blota::Topbar::InGame)
    	{
        	if(%bool == 0)
        	{
        	    $Blota::TopBarIcons -= 1;
				if(lagIcon.visible == 1)
				{
					$Blota::TopBarIcons -= 1;
					lagIcon.schedule(1,setVisible,1);
				}
				if(Blota_RTBIsTyping.visible == 1)
				{
					$Blota::TopBarIcons -= 1;
					Blota_RTBIsTyping.schedule(1,setVisible,1);
				}
            	//echo($Blota::TopBarIcons);
        	}
        	else if(%bool == 1)
        	{
         		$Blota::TopBarIcons += 1;
				//echo($Blota::TopBarIcons);
				HUD_SuperShift.setBitmap($Blota::ImagePath @ "topBarIcons/supershift.png");
				%xpos = getWord(Blota_DT_TopBar_StatusIcon.position, 0);
				%prex = 27*  $Blota::TopBarIcons;
				%x = %xpos - %prex; //Ish, how much bigger the icon is
				HUD_SuperShift.resize(%x, 2, 20, 20);
				Blota_TopBar.add(HUD_SuperShift);
        	}
        }
        parent::setVisible(%this, %bool);
    }

	function RTBCC_Session::updateStatus(%this,%status)
	{
		if(%Status == 1)
		{
		    $Blota::TopBarIcons += 1;
			//echo($Blota::TopBarIcons);
			%xpos = getWord(Blota_DT_TopBar_StatusIcon.position, 0);
			%prex = 27 * $Blota::TopBarIcons;
			%x = %xpos - %prex; //Ish, how much bigger the icon is
			Blota_RTBIsTyping.resize(%x, 2, 20, 20);
			Blota_RTBIsTyping.setVisible(1);
			Blota_TopBar.add(Blota_RTBIsTyping);
		}
		else if(%status == 2)
		{
			%xpos = getWord(Blota_DT_TopBar_StatusIcon.position, 0);
			%prex = 27 * $Blota::TopBarIcons;
			%x = %xpos - %prex; //Ish, how much bigger the icon is
			Blota_RTBIsTyping.resize(%x, 2, 20, 20);
			Blota_RTBIsTyping.setVisible(1);
			Blota_TopBar.add(Blota_RTBIsTyping);
		}
		else if(%Status == 0)
		{
			if($Blota::TopBarIcons == 0)
			{
				//$Blota::TopBarIcons -= 1;
			}
			else
			{
				$Blota::TopBarIcons -= 1;
			}

			Blota_RTBIsTyping.setVisible(0);
			if(lagIcon.visible == 1)
			{
				//$Blota::TopBarIcons -= 1;
				lagIcon.schedule(1,setVisible,1);
			}

			if(HUD_SuperShift.visible == 1)
			{
				//$Blota::TopBarIcons -= 1;
				HUD_SuperShift.schedule(1,setVisible,1);
			}
		}
		parent::updateStatus(%this,%status);
	}
};
activatePackage(Blota_DeskTop);