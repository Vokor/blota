
//sounds

new AudioProfile(Blota_HoverSound)
{
	fileName = "./sounds/hover.wav";
	description = AudioGui;
	preload = true;
};
new AudioProfile(Blota_clickSound)
{
	fileName = "./sounds/click.wav";
	description = AudioGui;
	preload = true;
};
new AudioProfile(Blota_notifSound)
{
	fileName = "./sounds/notif.wav";
	description = AudioGui;
	preload = true;
};
